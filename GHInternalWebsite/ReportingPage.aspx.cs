﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace GHInternalWebsite
{
    public partial class ReportingPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                string conn =
                ConfigurationManager.ConnectionStrings
                ["DataXchange"].ConnectionString;

                using (SqlConnection con = new SqlConnection(conn))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT clientid, Name FROM clients cl"))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        con.Open();
                        ddlSchool.DataSource = cmd.ExecuteReader();
                        ddlSchool.DataTextField = "Name";
                        ddlSchool.DataValueField = "ClientId";
                        ddlSchool.DataBind();
                        con.Close();

                    }
                    ddlSchool.SelectedValue = "--Select School--";

                }

            }
            
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            DateTime calStartDate = Convert.ToDateTime(calendarStartDate.Text);
            DateTime calEndDate = Convert.ToDateTime(calendarEndDate.Text);
            string school = ddlSchool.SelectedItem.Text;



            using (var context = new dataexchange_interactionsEntities())

            {
                calEndDate = calEndDate.AddDays(1);
                var query = (from c in context.five9_call
                             join ci in context.clients_items on c.campaignid equals ci.itemid
                             join i in context.interactions on c.interactionid equals i.interactionid
                             join i9 in context.interactions_five9 on c.interactionid equals i9.interactionid
                             join f9ca in context.five9_call_agent on c.callid equals f9ca.callid
                             join f9a in context.five9_agent on f9ca.agentid equals f9a.agentid
                             join fi in context.five9_item on c.campaignid equals fi.itemid
                             join cl in context.clients on ci.clientid equals cl.clientid
                             where cl.name == school && c.datestart >= calStartDate && c.datestart <= calEndDate
                             select new { cl.name,c.datestart,c.callid, i9.dispositionname, i.originator, i.destinator }).Distinct();


                DataTable dt = new DataTable();

                
                dt.Columns.Add("name");
                dt.Columns.Add("CallId");
                dt.Columns.Add("DateStart");
                //dt.Columns.Add("Campaign");
                dt.Columns.Add("Disposition");
                dt.Columns.Add("Originator");
                dt.Columns.Add("Destinator");


                foreach (var obj in query.ToList())
                {
                    DataRow dr = dt.NewRow();

                   
                    dr["name"] = obj.name;
                    dt.Rows.Add(dr);
                    dr["CallId"] = obj.callid;
                    dr["DateStart"] = obj.datestart;
                    //dr["Campaign"] = obj.CampaignName;
                    //dt.Rows.Add(dr);
                    dr["Disposition"] = obj.dispositionname;
                    dr["Originator"] = obj.originator;
                    dr["Destinator"] = obj.destinator;

                }

                //calendarStartDate.Text = "datestart";
                //calendarEndDate.Text = "datestart";
                //ddlSchool.Text = "name";
                //ddlSchool.DataValueField = "clientid";
                //DataView dv = dt.DefaultView;

                ////dv.RowFilter = "datestart >= " + calendarStartDate.Text + " and datestart <= " + calendarEndDate.Text;
                //dv.Sort = "datestart desc";
                //ddlSchool.DataBind();
                //ddlSchool.DataSource = dv;



                GridView1.DataSource = dt;
                GridView1.DataBind();

            }
          
        }
    }
}
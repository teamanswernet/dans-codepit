﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="School Info.aspx.cs" Inherits="GHInternalWebsite.School_Info" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="Content/StyleSheet.css" rel="stylesheet" />
     <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <style>

        .blogShort{ border-bottom:1px solid #ddd;}
.add{background: #333; padding: 10%; height: 300px;}

.nav-sidebar { 
    width: 100%;
    padding: 30px 0; 
    border-right: 1px solid #ddd;
}
.nav-sidebar a {
    color: #333;
    -webkit-transition: all 0.08s linear;
    -moz-transition: all 0.08s linear;
    -o-transition: all 0.08s linear;
    transition: all 0.08s linear;
}
.nav-sidebar .active a { 
    cursor: default;
    background-color: #0b56a8; 
    color: #fff; 
}
.nav-sidebar .active a:hover {
    background-color: #E50000;   
}
.nav-sidebar .text-overflow a,
.nav-sidebar .text-overflow .media-body {
    white-space: nowrap;
    overflow: hidden;
    -o-text-overflow: ellipsis;
    text-overflow: ellipsis; 
}


.btn-blog {
    color: #ffffff;
    background-color: #E50000;
    border-color: #E50000;
    border-radius:0;
    margin-bottom:10px
}
.btn-blog:hover,
.btn-blog:focus,
.btn-blog:active,
.btn-blog.active,
.open .dropdown-toggle.btn-blog {
    color: white;
    background-color:#0b56a8;
    border-color: #0b56a8;
}
article h2{color:#333333;}
h2{color:#0b56a8;}
 .margin10{margin-bottom:10px; margin-right:10px;}
 
 .container .text-style
{
  text-align: justify;
  line-height: 23px;
  margin: 0 13px 0 0;
  font-size: 19px;
 
}


    </style>

    <div id="wrapper">

            <div id="header">
            </div><!-- #header -->
            <nav class="navbar navbar-default navbar-fixed-top" style="border:none; background-color:white;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#divNav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="Content/Images/GHLogo.jpg" alt="GHLogo" height="35" />
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="#divNav">
                        <ul class="nav navbar-nav navbar-right" style="border:none; background-color:transparent;">
                            <li class="navbar-link">
                                <a href="ReportingPage.aspx">Reports</a>
                            </li>
                            <li class="navbar-link">
                                <a href="School Info.aspx">School Info</a>
                            </li>
                            <li class="navbar-link">
                                <a href="UserDashboard.aspx">Dashboard</a>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        <div id="divContentContainer" style="padding-top:5%; left:0;">
            <div class="container"  style="padding-left:0px; margin-left:0; padding-right:0px; padding-left:0px;">
	<div class="col-sm-2">
    <nav class="nav-sidebar">
		<ul class="nav tabs">
          <li class="active" role="presentation"><a href="#tab1" data-toggle="tab" role="tab">Rio Salado College</a></li>
          <li class="" role="presentation"><a href="#tab2" data-toggle="tab" role="tab">University of Alabama</a></li>
          <li class="" role="presentation"><a href="#tab3" data-toggle="tab" role="tab">Troy University</a></li>
          <li class="" role="presentation"><a href="#tab4" data-toggle="tab" role="tab">Lorem ipsum</a></li>
          <li class="" role="presentation"><a href="#tab5" data-toggle="tab" role="tab">Dolor asit amet</a></li>
          <li class="" role="presentation"><a href="#tab6" data-toggle="tab" role="tab">Stet clita</a></li>              
		</ul>
	</nav>
		<div><h3 class="add">Place for your add!</h3></div>
</div>
<!-- tab content -->
<div class="tab-content" style="position:inherit; display:flex; ">
<div role="tabpanel" class="tab-pane active text-style" id="tab1" style="float:right; width:1500px; height:650px;">
    <iframe src="TestPage.aspx" style="width:inherit; height:inherit; border:0px;">
</iframe>
</div>
<div role="tabpanel" class="tab-pane text-style" id="tab2" style="float:right; width:1500px; height:650px;">
    <iframe src="UniversityofAlabama.aspx" style="width:inherit; height:inherit; border:0px;">
</iframe>
</div>
<div role="tabpanel" class="tab-pane text-style" id="tab3">
  <h3>Stet clita</h3>
  <p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum 
    iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla 
    facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit 
    augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,</p>
    <hr>
    <div class="col-xs-6 col-md-3">
      <img src="http://placehold.it/150x150" class="img-rounded pull-right">
  </div>
</div>
</div>
    
    </div>
</div>

        </div><!-- #divContentContainer-->
    


            <div id="footer">
            </div><!-- #footer -->

        </div><!-- #wrapper -->
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
    <script>

        $(document).ready(function () {
            $(".nav-tabs a").click(function () {
                $(this).tab('show');
            });
        });

      //$(".nav li").removeClass("active");
      //  $(this).addClass("active");

      //  $('.nav li.active').removeClass('active');

      //  var $parent = $(this).parent();
      //  $parent.addClass('active');
      //  e.preventDefault();

      //  function activaTab(tab) {
      //      $('.navtabs a[href="#' + tab + '"]').tab('show');
      //  };

        //});


</script>
</html>

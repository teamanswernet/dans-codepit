
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/26/2017 12:10:37
-- Generated from EDMX file: C:\Users\Lyssa\source\repos\GHInternalWebsite\GHInternalWebsite\EDfDataSchool.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GHWeb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Entities'
CREATE TABLE [dbo].[Entities] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [FKAddress] int  NOT NULL,
    [PhoneNumber] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [WebAddress] nvarchar(max)  NOT NULL,
    [FKType] int  NOT NULL
);
GO

-- Creating table 'Addresses'
CREATE TABLE [dbo].[Addresses] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [Street] nvarchar(max)  NOT NULL,
    [City] nvarchar(max)  NOT NULL,
    [State] nvarchar(max)  NOT NULL,
    [Zip] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Departments'
CREATE TABLE [dbo].[Departments] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [FKSchool] int  NOT NULL,
    [PhoneNumber] nvarchar(max)  NOT NULL,
    [FKAddress] int  NOT NULL,
    [Property1] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'TypeLookups'
CREATE TABLE [dbo].[TypeLookups] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DepartmentTypes'
CREATE TABLE [dbo].[DepartmentTypes] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [FKDepartment] int  NOT NULL
);
GO

-- Creating table 'EntityNotes'
CREATE TABLE [dbo].[EntityNotes] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [FKEntity] int  NOT NULL,
    [Note] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Personnels'
CREATE TABLE [dbo].[Personnels] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [FKEntity] int  NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [JobTitle] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Extension] nvarchar(max)  NOT NULL,
    [Location] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Links'
CREATE TABLE [dbo].[Links] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [FKEntity] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [URL] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Profiles'
CREATE TABLE [dbo].[Profiles] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [FKEntity] int  NOT NULL
);
GO

-- Creating table 'ProfileLinks'
CREATE TABLE [dbo].[ProfileLinks] (
    [PrKey] int IDENTITY(1,1) NOT NULL,
    [FKProfile] int  NOT NULL,
    [FKLink] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [PrKey] in table 'Entities'
ALTER TABLE [dbo].[Entities]
ADD CONSTRAINT [PK_Entities]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- Creating primary key on [PrKey] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [PK_Addresses]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- Creating primary key on [PrKey] in table 'Departments'
ALTER TABLE [dbo].[Departments]
ADD CONSTRAINT [PK_Departments]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- Creating primary key on [PrKey] in table 'TypeLookups'
ALTER TABLE [dbo].[TypeLookups]
ADD CONSTRAINT [PK_TypeLookups]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- Creating primary key on [PrKey] in table 'DepartmentTypes'
ALTER TABLE [dbo].[DepartmentTypes]
ADD CONSTRAINT [PK_DepartmentTypes]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- Creating primary key on [PrKey] in table 'EntityNotes'
ALTER TABLE [dbo].[EntityNotes]
ADD CONSTRAINT [PK_EntityNotes]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- Creating primary key on [PrKey] in table 'Personnels'
ALTER TABLE [dbo].[Personnels]
ADD CONSTRAINT [PK_Personnels]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- Creating primary key on [PrKey] in table 'Links'
ALTER TABLE [dbo].[Links]
ADD CONSTRAINT [PK_Links]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- Creating primary key on [PrKey] in table 'Profiles'
ALTER TABLE [dbo].[Profiles]
ADD CONSTRAINT [PK_Profiles]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- Creating primary key on [PrKey] in table 'ProfileLinks'
ALTER TABLE [dbo].[ProfileLinks]
ADD CONSTRAINT [PK_ProfileLinks]
    PRIMARY KEY CLUSTERED ([PrKey] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [FKAddress] in table 'Entities'
ALTER TABLE [dbo].[Entities]
ADD CONSTRAINT [FK_AddressSchool]
    FOREIGN KEY ([FKAddress])
    REFERENCES [dbo].[Addresses]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AddressSchool'
CREATE INDEX [IX_FK_AddressSchool]
ON [dbo].[Entities]
    ([FKAddress]);
GO

-- Creating foreign key on [FKSchool] in table 'Departments'
ALTER TABLE [dbo].[Departments]
ADD CONSTRAINT [FK_FinancialAidSchool]
    FOREIGN KEY ([FKSchool])
    REFERENCES [dbo].[Entities]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FinancialAidSchool'
CREATE INDEX [IX_FK_FinancialAidSchool]
ON [dbo].[Departments]
    ([FKSchool]);
GO

-- Creating foreign key on [FKAddress] in table 'Departments'
ALTER TABLE [dbo].[Departments]
ADD CONSTRAINT [FK_FinancialAidAddress]
    FOREIGN KEY ([FKAddress])
    REFERENCES [dbo].[Addresses]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FinancialAidAddress'
CREATE INDEX [IX_FK_FinancialAidAddress]
ON [dbo].[Departments]
    ([FKAddress]);
GO

-- Creating foreign key on [FKType] in table 'Entities'
ALTER TABLE [dbo].[Entities]
ADD CONSTRAINT [FK_TypeLookupEntity]
    FOREIGN KEY ([FKType])
    REFERENCES [dbo].[TypeLookups]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TypeLookupEntity'
CREATE INDEX [IX_FK_TypeLookupEntity]
ON [dbo].[Entities]
    ([FKType]);
GO

-- Creating foreign key on [FKDepartment] in table 'DepartmentTypes'
ALTER TABLE [dbo].[DepartmentTypes]
ADD CONSTRAINT [FK_DepartmentTypeDepartment]
    FOREIGN KEY ([FKDepartment])
    REFERENCES [dbo].[Departments]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DepartmentTypeDepartment'
CREATE INDEX [IX_FK_DepartmentTypeDepartment]
ON [dbo].[DepartmentTypes]
    ([FKDepartment]);
GO

-- Creating foreign key on [FKEntity] in table 'EntityNotes'
ALTER TABLE [dbo].[EntityNotes]
ADD CONSTRAINT [FK_EntityNoteEntity]
    FOREIGN KEY ([FKEntity])
    REFERENCES [dbo].[Entities]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EntityNoteEntity'
CREATE INDEX [IX_FK_EntityNoteEntity]
ON [dbo].[EntityNotes]
    ([FKEntity]);
GO

-- Creating foreign key on [FKEntity] in table 'Personnels'
ALTER TABLE [dbo].[Personnels]
ADD CONSTRAINT [FK_PersonnelEntity]
    FOREIGN KEY ([FKEntity])
    REFERENCES [dbo].[Entities]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonnelEntity'
CREATE INDEX [IX_FK_PersonnelEntity]
ON [dbo].[Personnels]
    ([FKEntity]);
GO

-- Creating foreign key on [FKEntity] in table 'Links'
ALTER TABLE [dbo].[Links]
ADD CONSTRAINT [FK_LinkEntity]
    FOREIGN KEY ([FKEntity])
    REFERENCES [dbo].[Entities]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkEntity'
CREATE INDEX [IX_FK_LinkEntity]
ON [dbo].[Links]
    ([FKEntity]);
GO

-- Creating foreign key on [FKEntity] in table 'Profiles'
ALTER TABLE [dbo].[Profiles]
ADD CONSTRAINT [FK_ProfileEntity]
    FOREIGN KEY ([FKEntity])
    REFERENCES [dbo].[Entities]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProfileEntity'
CREATE INDEX [IX_FK_ProfileEntity]
ON [dbo].[Profiles]
    ([FKEntity]);
GO

-- Creating foreign key on [FKProfile] in table 'ProfileLinks'
ALTER TABLE [dbo].[ProfileLinks]
ADD CONSTRAINT [FK_ProfileLinkProfile]
    FOREIGN KEY ([FKProfile])
    REFERENCES [dbo].[Profiles]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProfileLinkProfile'
CREATE INDEX [IX_FK_ProfileLinkProfile]
ON [dbo].[ProfileLinks]
    ([FKProfile]);
GO

-- Creating foreign key on [FKLink] in table 'ProfileLinks'
ALTER TABLE [dbo].[ProfileLinks]
ADD CONSTRAINT [FK_ProfileLinkLink]
    FOREIGN KEY ([FKLink])
    REFERENCES [dbo].[Links]
        ([PrKey])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProfileLinkLink'
CREATE INDEX [IX_FK_ProfileLinkLink]
ON [dbo].[ProfileLinks]
    ([FKLink]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using GHInternalWebsite.Models;


namespace GHInternalWebsite
{
    public partial class UniversityofAlabama : System.Web.UI.Page
    {
            private DataSet ds;
            private DataConnection dc;

        protected void Page_Load(object sender, EventArgs e)
        {
            dc = new DataConnection();
            DataSet ds = dc.GetData(lblEntityName.Text);


            List<string> WebAddress = new List<string>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                WebAddress.Add(dr["WebAddress"].ToString());


            }

            //if (SalesOrderNumber == null) throw new Exception("Cant't Find ProcessInformation");


            lblWebAddress.Text = WebAddress[0];

        }
    }
}

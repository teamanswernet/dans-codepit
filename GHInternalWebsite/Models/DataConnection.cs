﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace GHInternalWebsite.Models
{
    public class DataConnection
    {
        public DataSet GetData(string EntityName)
        {
            Datasets.GHEntityDS ds = new Datasets.GHEntityDS();
            Datasets.GHEntityDSTableAdapters.SelectEntityInfoTableAdapter ad = new Datasets.GHEntityDSTableAdapters.SelectEntityInfoTableAdapter();
            ds.EnforceConstraints = false;

            try
            {
                ad.Fill(ds.SelectEntityInfo, EntityName);
            }
            catch (Exception)
            {

            }

            return ds;
        }


    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestPage.aspx.cs" Inherits="GHInternalWebsite.School_Pages.TestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/StyleSheet.css" rel="stylesheet" />
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
 <div id="wrapper">
        <style type="text/css" media="screen">
     * {
       margin: 0; 
       padding: 0;
     }

     div#banner { 
       position: absolute; 
       top: 0; 
       left: 0; 
       background-color: #DDEEEE; 
       width: 100%; 
     }
     div#banner-content { 
       width: 100%; 
       margin: 0 auto; 
       padding: 10px; 
     }
     div#main-content { 
       padding-top: 20px;
       padding-left: 5%;
       width:50%;
       height:50%;
    }

     table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
     *,
*:after,
*:before {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.clearfix:before,
.clearfix:after {
	content: " ";
	display: table;
}

.clearfix:after {
	clear: both;
}

body {
	font-family: sans-serif;
	background: white;
}

h1 {
	color: #ccc;
	/*text-align: center;*/
}

a {
  color: #ccc;
  text-decoration: none;
  outline: none;
}

/*TABS*/
.tab_container {
	width: 90%;
	position: relative;
}

input, section {
  clear: both;
  padding-top: 10px;
  display: none;
}

label {
  font-weight: 700;
  font-size: 12px;
  display: block;
  float: left;
  width: 20%;
  padding: 1.5em;
  color: #757575;
  cursor: pointer;
  text-decoration: none;
  text-align: center;
  background: #f0f0f0;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4,
#tab5:checked ~ #content5 {
  display: block;
  padding: 10px;
  background: #fff;
  /*color: #999;*/
}

.tab_container .tab-content p,
.tab_container .tab-content h4 {
  -webkit-animation: fadeInScale 0.7s ease-in-out;
  -moz-animation: fadeInScale 0.7s ease-in-out;
  animation: fadeInScale 0.7s ease-in-out;
}
.tab_container .tab-content h4  {
  /*text-align: center;*/
}

.tab_container [id^="tab"]:checked + label {
  background: #fff;
  box-shadow: inset 0 3px #0CE;
}

.tab_container [id^="tab"]:checked + label .fa {
  color: #0CE;
}

label .fa {
  font-size: 1.3em;
  margin: 0 0.4em 0 0;
}

/*Media query*/
@media only screen and (max-width: 930px) {
  label span {
    font-size: 14px;
  }
  label .fa {
    font-size: 14px;
  }
}

@media only screen and (max-width: 768px) {
  label span {
    display: none;
  }

  label .fa {
    font-size: 16px;
  }

  .tab_container {
    width: 98%;
  }
}

}
    </style>
            <div id="header">
            </div><!-- #header -->
            
        <div id="divContentContainer">
        <div id="banner">
    <div id="banner-content">

    </div>
  </div>
  <div id="main-content" style="width:70%; height:100%;">
        <img src="Content/Images/School Info Page/RioSalado_Logo-color.jpg" alt="GHLogo" height="80" />
      <div style=" position:inherit; height:500px;">
		<div class="tab_container">
			<input id="tab1" type="radio" name="tabs" checked>
			<label for="tab1"><i class="fa fa-code"></i><span>About</span></label>

			<input id="tab2" type="radio" name="tabs">
			<label for="tab2"><i class="fa fa-pencil-square-o"></i><span>GH Tools</span></label>

			<input id="tab3" type="radio" name="tabs">
			<label for="tab3"><i class="fa fa-bar-chart-o"></i><span>Contact</span></label>

            <input id="tab4" type="radio" name="tabs">
			<label for="tab4"><i class="fa fa-envelope-o"></i><span>Resources</span></label>

			<input id="tab5" type="radio" name="tabs">
			<label for="tab5"><i class="fa fa-envelope-o"></i><span>Other?</span></label>

			<section id="content1" class="tab-content">
				<h4>About</h4>
		      	<p>Rio Salado College is a community college headquartered in Tempe, Arizona. It is accredited by The Higher Learning Commission of the North Central Association of Colleges and Schools and offers associate degree and certificate programs in online, in-person, and hybrid formats.</p>
        <div style="position:inherit; height:auto; "><div style="width:50%; float:left; padding-top:20px;">
<h4>Quick Facts</h4>
<table>
       <tr>
    <td><b>Name:</b></td>
    <td><asp:label id="lblEntityName" runat="server" ClientIDMode="static" >Rio Salado College</asp:label></td>

  </tr>
      <tr>
    <td><b>Web Address:</b></td>
    <td><asp:label id="lblWebAddress" runat="server" ClientIDMode="static" ></asp:label></td>

  </tr>
  <tr>
    <td><b>Population:</b></td>
    <td>60,000</td>

  </tr>
  <tr>
    <td><b>Degree Programs:</b></td>
    <td>Associates, Certificates</td>

  </tr>
  <tr>
    <td><b>GH Services offered:</b></td>
    <td>General Helpline, Enrollment, Retention, Student Recruitment</td>

  </tr>

</table>
                </div><img src="Content/Images/School Info Page/mascot-open-hands.png" style="float:right; padding-right:20px;"/>
</div>
		      	
			</section>

			<section id="content2" class="tab-content">
				<h4>GH Tools</h4>
		      	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		      	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		      	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
		      
			</section>

			<section id="content3" class="tab-content">
				<h4>Contact</h4>
		      	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		      	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		      	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		      	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		      	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		      	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		    
			</section>

            
			<section id="content4" class="tab-content">
				<h4>Headline 4</h4>
		      	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		      	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		      	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
		      	
			</section>

			<section id="content5" class="tab-content">
				<h4>Headline 5</h4>
		      	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		      	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		      	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
		      	
			</section>
		</div>
      </div>

  </div>
            </div><!-- #divContentContainer-->
    


<%--            <div id="footer">
            </div><!-- #footer -->--%>

        </div><!-- #wrapper -->
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>

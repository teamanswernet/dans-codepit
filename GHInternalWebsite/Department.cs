//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GHInternalWebsite
{
    using System;
    using System.Collections.Generic;
    
    public partial class Department
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Department()
        {
            this.DepartmentType = new HashSet<DepartmentType>();
        }
    
        public int PrKey { get; set; }
        public int FKSchool { get; set; }
        public string PhoneNumber { get; set; }
        public int FKAddress { get; set; }
        public string Property1 { get; set; }
        public string Name { get; set; }
    
        public virtual Entity Schools { get; set; }
        public virtual Address Addresses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DepartmentType> DepartmentType { get; set; }
    }
}

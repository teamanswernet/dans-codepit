﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportingPage.aspx.cs" Inherits="GHInternalWebsite.ReportingPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-2.1.4.js"></script>
    <script src="Scripts/bootstrap.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="Content/StyleSheet.css" rel="stylesheet" />
    <meta charset="utf-8" />
</head>
<body>
    <form id="form1" runat="server">

        <style>
            .with-nav-tabs.panel-default .nav-tabs > li > a,
            .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
            .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
                color: #777;
            }

                .with-nav-tabs.panel-default .nav-tabs > .open > a,
                .with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
                .with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
                .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
                .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
                    color: #777;
                    background-color: #ddd;
                    border-color: transparent;
                }

            .with-nav-tabs.panel-default .nav-tabs > li.active > a,
            .with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
            .with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
                color: #555;
                background-color: #fff;
                border-color: #ddd;
                border-bottom-color: transparent;
            }

            .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
                background-color: #f5f5f5;
                border-color: #ddd;
            }

                .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
                    color: #777;
                }

                    .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
                    .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
                        background-color: #ddd;
                    }

                .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
                .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
                .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
                    color: #fff;
                    background-color: #555;
                }
        </style>
        <div id="wrapper">

            <div id="header">
            </div>
            <!-- #header -->
            <nav class="navbar navbar-default navbar-fixed-top" style="border: none; background-color: white;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#divNav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/Default.aspx">
                            <img src="Content/Images/GHLogo.jpg" alt="GHLogo" height="35" />
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="#divNav">
                        <ul class="nav navbar-nav navbar-right" style="border: none; background-color: transparent;">
                            <li class="navbar-link">
                                <a href="ReportingPage.aspx">Reports</a>
                            </li>
                            <li class="navbar-link">
                                <a href="School Info.aspx">School Info</a>
                            </li>
                            <li class="navbar-link">
                                <a href="UserDashboard.aspx">Dashboard</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>

            <div id="content" style="padding-top: 3%; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;">
                <div id="divReporting">
                    <h1 style="text-align: center;">Greenwoodhall Reporting</h1>
                </div>

                <div id="divSearchBoxWrap" style="height: 150px;">

                    <div id="divSearchBox" class="well well-lg" style="width: inherit; height: inherit; position: inherit;">

                        <div id="divSearchBoxContent">
                            <div id="divSBContent1" style="width: 50%; float: left; margin-right: 5%;">
                                <p>Call Dates</p>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">Start Date</button>
                                            </span>
                                            <asp:TextBox CssClass="form-control" ClientIDMode="Static" ID="calendarStartDate" runat="server" />
                                        </div>
                                        <!-- /input-group -->

                                        <!-- /input-group -->
                                    </div>
                                    <!-- /.col-lg-6 -->

                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">End Date</button>
                                            </span>
                                            <asp:TextBox CssClass="form-control" ClientIDMode="Static" ID="calendarEndDate" runat="server" />

                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <!-- /.col-lg-6 -->
                                </div>
                                <!-- /.row -->
                            </div>

                            <div id="divSBContent2" style="height: inherit;">
                                <p>Client</p>

                                <div>
                                    <div>

                                        <%--                                        <button style="position:absolute;" class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <span id="selected">Choose option</span><span class="caret"></span>
                                        </button>--%>
                                        <div>

                                            <asp:DropDownList Style="position: inherit;" AutoPostBack="false" ClientIDMode="static" ID="ddlSchool" runat="server" Width="120px" ForeColor="black" Font-Names="Arial" CssClass="ddl">
                                            </asp:DropDownList>
                                            <asp:Button class="btn btn-default" ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />
                                        </div>

                                        <%--                                    <ul id="ddltest" style="position:inherit;" class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>--%>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6" style="width: 100%;">
                        <div class="panel with-nav-tabs panel-default">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs" id="datatabs">
                                    <li class="active"><a href="#tab1default" data-toggle="tab">Data</a></li>
                                    <li class=""><a href="#tab2default" data-toggle="tab">Dashboard</a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab1default">

                                        <div style="height: 500px; overflow: auto;">
                                            <asp:GridView ID="GridView1" runat="server" Style="width: 100%;">
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab2default">
                                        dfskd;flsdlk;
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="position: fixed; width: auto; padding-top: 5%; width: 100%;">
                        <hr />


                    </div>
                </div>

            </div>
            <div id="footer" style="position: fixed;">
            </div>
            <!-- #footer -->

        </div>
        <!-- #wrapper -->

        <script>
            $(document).ready(function () {

                $("#calendarStartDate").datepicker({ dateFormat: "yy/mm/dd" });
                $("#calendarEndDate").datepicker({ dateFormat: "yy/mm/dd" });

            });
            $('#datatabs a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            });
        </script>
        <div>
        </div>
    </form>
</body>
</html>

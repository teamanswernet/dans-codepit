﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GHInternalWebsite
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class dataexchange_interactionsEntities : DbContext
    {
        public dataexchange_interactionsEntities()
            : base("name=dataexchange_interactionsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<client> clients { get; set; }
        public virtual DbSet<clients_items> clients_items { get; set; }
        public virtual DbSet<five9_agent> five9_agent { get; set; }
        public virtual DbSet<five9_call> five9_call { get; set; }
        public virtual DbSet<five9_call_agent> five9_call_agent { get; set; }
        public virtual DbSet<five9_call_counts> five9_call_counts { get; set; }
        public virtual DbSet<five9_call_disposition> five9_call_disposition { get; set; }
        public virtual DbSet<five9_call_note> five9_call_note { get; set; }
        public virtual DbSet<five9_call_recording> five9_call_recording { get; set; }
        public virtual DbSet<five9_call_segment> five9_call_segment { get; set; }
        public virtual DbSet<five9_call_time> five9_call_time { get; set; }
        public virtual DbSet<five9_item> five9_item { get; set; }
        public virtual DbSet<interaction> interactions { get; set; }
        public virtual DbSet<interactions_arc> interactions_arc { get; set; }
        public virtual DbSet<interactions_five9> interactions_five9 { get; set; }
        public virtual DbSet<interactions_five9_callhandling> interactions_five9_callhandling { get; set; }
    }
}

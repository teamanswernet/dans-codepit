﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserDashboard.aspx.cs" Inherits="GHInternalWebsite.UserDashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous" />
    <link href="Content/DashboardStyleSheet.css" rel="stylesheet" />

    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <body class="home">
            <div id="header">
            </div>
            <!-- #header -->
            <nav class="navbar navbar-default navbar-fixed-top" style="border: none; background-color: white;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#divNav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="Content/Images/GHLogo.jpg" alt="GHLogo" height="35" />
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="#divNav">
                        <ul class="nav navbar-nav navbar-right" style="border: none; background-color: transparent;">
                            <li class="navbar-link">
                                <a href="ReportingPage.aspx">Reports</a>
                            </li>
                            <li class="navbar-link">
                                <a href="School Info.aspx">School Info</a>
                            </li>
                            <li class="navbar-link">
                                <a href="UserDashboard.aspx">Dashboard</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <div class="container-fluid display-table" style="margin-top: 5%;">
                <div class="row display-table-row">
                    <div class="col-md-2 col-sm-1 hidden-xs display-table-cell v-align box" id="navigation">
                        <%--  <div class="logo">
                    <a href="home.html"><img src="http://jskrishna.com/work/merkury/images/logo.png" alt="merkery_logo" class="hidden-xs hidden-sm">
                        <img src="http://jskrishna.com/work/merkury/images/circle-logo.png" alt="merkery_logo" class="visible-xs visible-sm circle-logo">
                    </a>
                </div>--%>
                        <div class="navi">
                            <ul>
                                <li class="active"><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Home</span></a></li>
                                <li><a href="#"><i class="fa fa-tasks" aria-hidden="true"></i><span class="hidden-xs hidden-sm">???</span></a></li>
                                <li><a href="#"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-xs hidden-sm">???</span></a></li>
                                <li><a href="#TrainingDocs"><i class="fa fa-folder" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Training </span></a></li>
                                <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Calender</span></a></li>
                                <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Settings</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-11 display-table-cell v-align">
                        <!--<button type="button" class="slide-toggle">Slide Toggle</button> -->
                        <div class="row">
                            <header>
                                <div class="col-md-7">
                                    <nav class="navbar-default pull-left">
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#side-menu" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                    </nav>
                                    <div class="search hidden-xs hidden-sm">
                                        <input type="text" placeholder="Search" id="search">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="header-rightside">
                                        <ul class="list-inline header-top pull-right">
                                            <li class="hidden-xs"><a href="#" class="add-project" data-toggle="modal" data-target="#add_project">Add Project</a></li>
                                            <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                            <li>
                                                <a href="#" class="icon-info">
                                                    <i class="fa fa-bell" aria-hidden="true"></i>
                                                    <span class="label label-primary">3</span>
                                                </a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <img src="http://jskrishna.com/work/merkury/images/user-pic.jpg" alt="user">
                                                    <b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <div class="navbar-content">
                                                            <span>NAME</span>
                                                            <p class="text-muted small">
                                                                EMAIL
                                                            </p>
                                                            <div class="divider">
                                                            </div>
                                                            <a href="#" class="view btn-sm active">View Profile</a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </header>
                        </div>
                        <div class="user-dashboard">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active text-style" id="#Home" style="float: right; width: 1500px; height: 650px;">
                                    <h1>Hello, NAME</h1>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5 col-xs-12 gutter">
                                            <%--                            <div class="sales">
                                <h3>Your Sale</h3>

                                <div class="btn-group">
                                    <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span>Period:</span> Last Year
                                    </button>
                                    <div class="dropdown-menu">
                                        <a href="#">2012</a>
                                        <a href="#">2014</a>
                                        <a href="#">2015</a>
                                        <a href="#">2016</a>
                                    </div>
                                </div>
                            </div>--%>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-12 gutter">

                                            <%--                          <div class="sales report">
                                <h3>Report</h3>
                                <div class="btn-group">
                                    <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span>Period:</span> Last Year
                                    </button>
                                    <div class="dropdown-menu">
                                        <a href="#">2012</a>
                                        <a href="#">2014</a>
                                        <a href="#">2015</a>
                                        <a href="#">2016</a>
                                    </div>
                                </div>
                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane text-style" id="#TrainingDocs" style="float: right; width: 1500px; height: 650px;">
                                    <iframe src="UniversityofAlabama.aspx" style="width: inherit; height: inherit; border: 0px;"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



            <!-- Modal -->
            <div id="add_project" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header login-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Add Project</h4>
                        </div>
                        <div class="modal-body">
                            <input type="text" placeholder="Project Title" name="name">
                            <input type="text" placeholder="Post of Post" name="mail">
                            <input type="text" placeholder="Author" name="passsword">
                            <textarea placeholder="Desicrption"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="cancel" data-dismiss="modal">Close</button>
                            <button type="button" class="add-project" data-dismiss="modal">Save</button>
                        </div>
                    </div>

                </div>
            </div>

        </body>
    </form>

</body>
<script>
    $(document).ready(function () {
        $('[data-toggle="offcanvas"]').click(function () {
            $("#navigation").toggleClass("hidden-xs");
        });
        $(".navi a").click(function () {
            $(this).tab('show');
        });
    });



</script>
</html>

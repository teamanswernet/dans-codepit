﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tools.aspx.cs" Inherits="GHInternalWebsite.Tools" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/StyleSheet.css" rel="stylesheet" />
    <meta charset="utf-8" />
    <title></title>
</head>
<body>

    <div id="wrapper">

            <div id="header">
            </div><!-- #header -->
            <nav class="navbar navbar-default navbar-fixed-top" style="border:none; background-color:white;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#divNav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="Content/Images/GHLogo.jpg" alt="GHLogo" height="35" />
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="#divNav">
                        <ul class="nav navbar-nav navbar-right" style="border:none; background-color:transparent;">
                            <li class="navbar-link">
                                <a href="/ReportingPage.aspx">Reports</a>
                            </li>
                            <li class="navbar-link">
                                <a href="/School Info.aspx">School Info</a>
                            </li>
                            <li class="navbar-link">
                                <a href="#">Tools</a>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        <div id="divContentContainer">
            <div style="border: 4px solid black; width:1500px; height:700px; margin-top:5%; margin-left:5%" ><div style="float:left; width:50%; height:100%; border:dashed 3px black;"><h3>GreenwoodHall Employee Resources</h3>
                <table>
                    <tr>
                        <td><a href="https://access.paylocity.com/">Paylocity</a>
</td>
                    </tr>
                    <tr>
                        <td><a href="https://www.office.com/">Office 365</a>
</td>
                    </tr>
                    <tr>
                        <td><a href="https://login.five9.com/">Five9</a>
</td>
                    </tr>
                </table>

                                                                                                             </div>
           <div style="float:right; width:50%; height:100%; border:dashed 3px black;"><h3>SCHOOL Resources</h3>

           </div>

            </div>
            </div><!-- #divContentContainer-->
    


            <div id="footer">
            </div><!-- #footer -->

        </div><!-- #wrapper -->
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>


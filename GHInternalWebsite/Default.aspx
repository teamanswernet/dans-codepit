﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/StyleSheet.css" rel="stylesheet" />
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
                        <form id="form1" runat="server">

    <div id="wrapper">

            <div id="header">
            </div><!-- #header -->
            <nav class="navbar navbar-default navbar-fixed-top" style="border:none; background-color:white;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#divNav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="Content/Images/GHLogo.jpg" alt="GHLogo" height="35" />
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="#divNav">
                        <ul class="nav navbar-nav navbar-right" style="border:none; background-color:transparent;">
                            <li class="navbar-link">
                                <a href="ReportingPage.aspx">Reports</a>
                            </li>
                            <li class="navbar-link">
                                <a href="School Info.aspx">School Info</a>
                            </li>
                            <li class="navbar-link">
                                <a href="UserDashboard.aspx">Dashboard</a>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        <div id="divContentContainer">
            <div id="content">
                <div id="divJumbo" class="jumbotron">
                    <div style="padding-top:5%; padding-bottom:5%;">
                        <h4 style="color:white; font-family:Verdana; font-size:65px; text-align:center;">Greenwood Hall<br /> Education Solutions</h4>
                        <div id="btnWrap" style="width:200px; margin: 0 auto;">
                            <button type="button" class="btn btn-primary" style="float:left;">Click This</button>
                            <button type="button" class="btn btn-primary" style="float:right;">Click That</button>
                        </div>
                    </div>
            </div>
     
            </div><!-- #content --><div id="whatsnew" style="padding-left:11%;"><h1 style="font-size:24px;">What's new at Greenwood Hall...</h1></div>

            <div id="divWrapperLower" style="position:inherit; display:flex; width:1200px;">
                <div id="divBox1" style="position:inherit; padding-left:15%; "><div class="row">
                    <div style="width:auto;"class="col-sm-6 col-md-4">
                        <div style="height:400px;" class="thumbnail">
                            <img src="Content/Images/apple-256263_960_720.jpg" alt="Greenwoodhall"/>
                            <div class="caption">
                                <h4>Read about Greenwoodhall's latest blank!</h4>
                                <p>Blankedy blank blank</p>
                                <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                            </div>
                        </div>
                    </div>
                </div></div>
                <div id="divBox2" style="position:inherit;">
                <div class="row">
    <div style="width:auto;" class="col-sm-6 col-md-4">
        <div style="height:400px;"class="thumbnail">
            <img style="width:200px;"src="Content/Images/school-2276269_960_720.jpg" alt="Greenwoodhall">
            <div class="caption">
                <h4>Read about Greenwoodhall's latest blank!</h4>
                <p>Blankedy blank blank</p>
                <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
            </div>
        </div>
    </div>
</div>
                    </div>

                  <div class="container" style="margin-top:30px; margin-left:200px;">
<div style="width:400px; height:300px;">
         <h4 style="font-size: medium">You are logged in!</h4>

</div>
</div>
            </div>
            </div>
          <!-- #divContentContainer-->
    


            <div id="footer">
            </div><!-- #footer -->

        </div><!-- #wrapper -->
        <div>
        </div>
    </form>
</body>
</html>

﻿using System.Linq;

namespace OpsLibrary.Abstract
{
    public interface iClientRepository
    {
        IQueryable<client> Clients { get; }
    }

    public interface iCallTimeRepository
    {
        IQueryable<five9_call_time> CallTimes { get; }
    }

    public interface iCallAgentRepository
    {
        IQueryable<five9_call_agent> CallAgents { get; }
    }

    public interface iAgentRepository
    {
        IQueryable<five9_agent> Agents { get; }
    }

    public interface iCallRepository
    {
        IQueryable<five9_call> Calls { get; }
    }

    public interface iInteractionRepository
    {
        IQueryable<interaction> Interactions { get; }
    }

    public interface iItemRepository
    {
        IQueryable<five9_item> Items { get; }
    }

    public interface iCallSegmentRepository
    {
        IQueryable<five9_call_segment> CallSegments { get; }
    }

    public interface iCallRecordingRepository
    {
        IQueryable<five9_call_recording> CallRecordings { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpsLibrary.Entities
{
    public class RecordModel
    {
        public long CallId { get; set; }
        public string AgentName { get; set; }
        public string Campaign { get; set; }
        public string Disposition { get; set; }
        public int Recording { get; set; }
        public string Orginator { get; set; }
        public string Destinator { get; set; }
        public DateTime CallDate { get; set; }
        public int HandleTime { get; set; }
        public int HoldTime { get; set; }
        public int QueueTime { get; set; }
        public int RingTime { get; set; }
        public int TalkTime { get; set; }
        public int IVRTime { get; set; }
        public int Wrapup { get; set; }
    }


    public class ReaderConnection
    {
        private SqlConnection conn;
        private SqlCommand command;

        public ReaderConnection()
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DataExchange"].ConnectionString);
        }

        /// <summary>
        /// Gets the Call log information with a datareader.  Depreciated since im not using a linq query to get the data with EFGetCallLog
        /// </summary>
        /// <param name="school"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<RecordModel> GetCallLog(string school, DateTime startDate, DateTime endDate)
        {
            List<RecordModel> tempList = new List<RecordModel>();

            string query = "select distinct c.callid as 'Callid', camp.name as 'Campaign', (select top 1 dispositionname from interactions_five9 where callid = c.callid) as 'Disposition', cc.recordings as 'Recording Count', i.originator as 'Originator', i.destinator as 'Destinator', c.datestart as 'Call Date', ct.ring_time as 'RingTime', ct.queue_time as 'QueueTime',  ct.talk_time as 'TalkTime', ct.hold_time as 'HoldTime', ct.handle_time as 'HandleTime', ct.ivr_time as 'IVR Time', ct.wrapup_time as 'Wrapup Time' from five9_call c join five9_item camp on c.campaignid = camp.itemid join five9_call_counts cc on c.callid = cc.callid join interactions i on c.interactionid = i.interactionid join clients_items ci on ci.itemid = c.campaignid join clients cl on ci.clientid = cl.clientid join five9_call_time ct on c.callid = ct.callid where cl.name like '"+ school + "' and c.datestart between '" + startDate.ToShortDateString() + "' and '" + endDate.AddDays(1).ToShortDateString() + "'";

            command = new SqlCommand(query, conn);
            command.CommandType = CommandType.Text;

            try
            {
                conn.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        RecordModel temp = new RecordModel()
                        {
                            CallId = long.Parse(reader["Callid"].ToString()),
                            Campaign = reader["Campaign"].ToString(),
                            Disposition = reader["Disposition"].ToString(),
                            Recording = int.Parse(reader["Recording Count"].ToString()),
                            Orginator = reader["Originator"].ToString(),
                            Destinator = reader["Destinator"].ToString(),
                            CallDate = DateTime.Parse(reader["Call Date"].ToString()).AddHours(3),
                            HandleTime = int.Parse(reader["HandleTime"].ToString()),
                            RingTime = int.Parse(reader["RingTime"].ToString()),
                            QueueTime = int.Parse(reader["QueueTime"].ToString()),
                            TalkTime = int.Parse(reader["TalkTime"].ToString()),
                            HoldTime = int.Parse(reader["HoldTime"].ToString()),
                            IVRTime = int.Parse(reader["IVR Time"].ToString()),
                            Wrapup = int.Parse(reader["Wrapup Time"].ToString())
                        };

                        tempList.Add(temp);
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                conn.Close();
            }

            return tempList;
        }

        public List<RecordModel> EFGetCallLog(string school, DateTime startDate, DateTime endDate, TimeSpan offset, bool isDemo)
        {
            List<RecordModel> templist = new List<RecordModel>();

            try
            {

                using (var context = new EFDataExchange())
                {
                    endDate = endDate.AddDays(1);
                    var query = from c in context.five9_call
                                join camp in context.five9_item on c.campaignid equals camp.itemid
                                join cli in context.clients_items on c.campaignid equals cli.itemid
                                join cl in context.clients on cli.clientid equals cl.clientid
                                join cc in context.five9_call_counts on c.callid equals cc.callid
                                join i in context.interactions on c.interactionid equals i.interactionid
                                join ct in context.five9_call_time on c.callid equals ct.callid
                                join disp in context.interactions_five9 on i.interactionid equals disp.interactionid
                                join ca in context.five9_call_agent on c.callid equals ca.callid
                                join a in context.five9_agent on ca.agentid equals a.agentid
                                where cl.name == school && c.datestart >= startDate && c.datestart <= endDate
                                select new { c.callid, camp.name, disp.dispositionname, cc.recordings, i.originator, i.destinator, c.datestart, ct.ring_time, ct.queue_time, ct.talk_time, ct.hold_time, ct.handle_time, ct.ivr_time, ct.wrapup_time, a.fullname };


                    foreach (var item in query.ToList())
                    {
                        if (isDemo)
                        {

                            RecordModel temp = new RecordModel()
                            {
                                CallId = item.callid,
                                Campaign = "Demo University",
                                Recording = item.recordings,
                                Orginator = item.originator,
                                Destinator = item.destinator,
                                CallDate = item.datestart.AddHours(offset.Hours),
                                RingTime = item.ring_time,
                                QueueTime = item.queue_time,
                                TalkTime = item.talk_time,
                                HoldTime = item.hold_time,
                                HandleTime = item.handle_time,
                                IVRTime = item.ivr_time,
                                Wrapup = item.wrapup_time,
                                Disposition = item.dispositionname,
                                AgentName = item.fullname
                            };
                            templist.Add(temp);
                        }
                        else
                        {
                            RecordModel temp = new RecordModel()
                            {
                                CallId = item.callid,
                                Campaign = item.name,
                                Recording = item.recordings,
                                Orginator = item.originator,
                                Destinator = item.destinator,
                                CallDate = item.datestart.AddHours(offset.Hours),
                                RingTime = item.ring_time,
                                QueueTime = item.queue_time,
                                TalkTime = item.talk_time,
                                HoldTime = item.hold_time,
                                HandleTime = item.handle_time,
                                IVRTime = item.ivr_time,
                                Wrapup = item.wrapup_time,
                                Disposition = item.dispositionname,
                                AgentName = item.fullname
                            };
                            templist.Add(temp);
                        }

                       

                    }

                }
            }
            catch (Exception)
            {

            }

            return templist.Distinct().ToList();
        }
    
        public List<CallVolumeDate> GetCallVolumeforDates(DateTime qDate, DateTime eDate, string school)
        {
            List<CallVolumeDate> tempList = new List<CallVolumeDate>();

            try
            {
                using (var context = new EFDataExchange())
                {
                    eDate = eDate.AddDays(1);
                    var query = from c in context.five9_call
                                join ci in context.clients_items on c.campaignid equals ci.itemid
                                join cl in context.clients on ci.clientid equals cl.clientid
                                where c.datestart >= qDate && c.datestart < eDate && cl.name.Equals(school)
                                select new { c.callid, c.datestart };


                    foreach(var obj in query.ToList())
                    {
                        CallVolumeDate sTemp = new CallVolumeDate() { CallId = obj.callid, Date = obj.datestart };
                        tempList.Add(sTemp);
                    }
                    
                }
            }
            catch(Exception)
            {

            }

            return tempList;
        }
    }//end class
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpsLibrary.Entities
{
    public class CallTimeViewModel
    {
        public TimeSpan Length { get; set; }
        public TimeSpan ConferenceTime { get; set; }
        public TimeSpan ConsultTime { get; set; }
        public TimeSpan HandleTime { get; set; }
        public TimeSpan HoldTime { get; set; }
        public TimeSpan IVRTime { get; set; }
        public TimeSpan QueueTime { get; set; }
        public TimeSpan RingTime { get; set; }
        public TimeSpan TalkTime { get; set; }
        public TimeSpan Wrapup { get; set; }
    }

    public class InteractionDetailsModel
    {
        public long CallId { get; set; }
        public string SessionId { get; set; }
        public DateTime CallStart { get; set; }
        public DateTime CallEnd { get; set; }
        public string Originator { get; set; }
        public string Destinator { get; set; }
        public string CallType { get; set; }
    }

    public class CallSegmentViewModel
    {
        public string CallType { get; set; }
        public string Result { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public TimeSpan Duration { get; set; }
    }

    public class DailyResultModel
    {
        public DailySummaryViewModel DailySum { get; set; }
        public DailyPercentViewModel DailyPrc { get; set; }
        public List<HourlyCallStatViewModel> HourStats { get; set; }
    }

    public class DailySummaryViewModel
    {
        public int AbandonTotal { get; set; }
        public int AnsUnd90 { get; set; }
        public int AnsUnd3min { get; set; }
        public int CallTrans { get; set; }
        public int CallTotal { get; set; }
    }

    public class DailyPercentViewModel
    {
        public double AbandonPerc { get; set; }
        public double Under90 { get; set; }
        public double Under3min { get; set; }
        public double TransPrec { get; set; }
    }

    public class HourlyCallStatViewModel
    {
        public string CallHour { get; set; }
        public int CallTotal { get; set; }
        public int AgentCount { get; set; }
        public double AbandonPer { get; set; }
        public TimeSpan AvgRingTime { get; set; }
        public TimeSpan AvgQueueTime { get; set; }
        public TimeSpan AvgTalkTime { get; set; }
        public TimeSpan AvgHoldTime { get; set; }
        public int TransNum { get; set; }
        public TimeSpan AvgHandleTime { get; set; }
    }

    public class PieChartViewModel
    {
        public string Disposition { get; set; }
        public double Percentage { get; set; }
        public int Count { get; set; }
    }

    public class UserViewModel
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }

    public class QueryObj
    {
        public string school { get; set; }
        public DateTime sDate { get; set; }
        public DateTime eDate { get; set; }
    }

    public class CallVolumeDate
    {
        public DateTime Date { get; set; }
        public string DayOfWeek { get; set; }
        public long CallId { get; set; }
        public int CallCount { get; set; }
    }

}
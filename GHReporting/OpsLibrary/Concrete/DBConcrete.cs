﻿using OpsLibrary.Abstract;
using System.Linq;

namespace OpsLibrary.Concrete
{
    public class EFClientRepository : iClientRepository
    {
        private EFDataExchange context = new EFDataExchange();

        public IQueryable<client> Clients
        {
            get { return context.clients; }
        }
    }//end class

    public class EFCallTimeRepository : iCallTimeRepository
    {
        private EFDataExchange context = new EFDataExchange();

        public IQueryable<five9_call_time> CallTimes
        {
         get { return context.five9_call_time; }   
        }
    }

    public class EFCallAgentRepository :iCallAgentRepository
    {
        private EFDataExchange context = new EFDataExchange();

        public IQueryable<five9_call_agent> CallAgents
        {
            get { return context.five9_call_agent; }
        }
    }

    public class EFAgentRepository : iAgentRepository
    {
        private EFDataExchange context = new EFDataExchange();

        public IQueryable<five9_agent> Agents
        {
            get { return context.five9_agent;  }
        }
    }

    public class EFCallRepository : iCallRepository
    {
        private EFDataExchange context = new EFDataExchange();

        public IQueryable<five9_call> Calls
        {
            get { return context.five9_call; }
        }
    }

    public class EFInteractions : iInteractionRepository
    {
        private EFDataExchange context = new EFDataExchange();

        public IQueryable<interaction> Interactions
        {
            get { return context.interactions; }
        }
    }

    public class EFItemRepository : iItemRepository
    {
        private EFDataExchange context = new EFDataExchange();

        public IQueryable<five9_item> Items
        {
            get { return context.five9_item; }
        }
    }

    public class EFCallSegementRepository : iCallSegmentRepository
    {
        private EFDataExchange context = new EFDataExchange();

        public IQueryable<five9_call_segment> CallSegments
        {
            get { return context.five9_call_segment; }
        }
    }

    public class EFCallRecordingRepository : iCallRecordingRepository
    {
        private EFDataExchange context = new EFDataExchange();

        public IQueryable<five9_call_recording> CallRecordings
        {
            get { return context.five9_call_recording; }
        }
    }
}

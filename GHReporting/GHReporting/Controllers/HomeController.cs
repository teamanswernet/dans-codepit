﻿using System.Web.Security;
using OpsLibrary;
using OpsLibrary.Abstract;
using OpsLibrary.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Globalization;

namespace GHReporting.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        #region Objects
        private iClientRepository clientRepo;
        private iCallTimeRepository ctRepo;
        private iCallAgentRepository caRepo;
        private iAgentRepository aRepo;
        private iCallRepository callRepo;
        private iInteractionRepository interactionRepo;
        private iItemRepository itemRepo;
        private iCallSegmentRepository callSegRepo;
        private iCallRecordingRepository recordingRepository;
        private TimeSpan offset;
        #endregion

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private RoleManager<IdentityRole> roleManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        //constructor
        public HomeController(iClientRepository clientRepository, iCallTimeRepository callTimeRepository, iCallAgentRepository cAgentRepository, iAgentRepository agentRepository, iCallRepository cRepo, iInteractionRepository iRepo, iItemRepository itemRepository, iCallSegmentRepository csRepo, iCallRecordingRepository cRecRepo)
        {
            clientRepo = clientRepository;
            ctRepo = callTimeRepository;
            caRepo = cAgentRepository;
            aRepo = agentRepository;
            callRepo = cRepo;
            interactionRepo = iRepo;
            itemRepo = itemRepository;
            callSegRepo = csRepo;
            recordingRepository = cRecRepo;
        }

        public async Task<ActionResult> Index()
        {
            List<string> clients = new List<string>();

            if (User.IsInRole("GH Sales"))
            {
                clients.Add("Demo University");        //added to trigger the demo information
            }
            else if (!User.IsInRole("Administrator"))
            {
                var userid = User.Identity.GetUserId();
                var roles = await UserManager.GetRolesAsync(userid);

                clients.Add(roles.First());
            }
            else
            {
                //get a list of clients from the DB
                clients = clientRepo.Clients.Select(x => x.name).ToList();
                clients.Add("Demo University");
            }

            ViewData["Clients"] = clients;

            ViewBag.StartDate = DateTime.Now.AddDays(-1).ToShortDateString();
            ViewBag.EndDate = DateTime.Now.AddDays(-1).ToShortDateString();

            return View();
        }

        [HttpPost]
        public ViewResult GetData(FormCollection form)
        {
            ReaderConnection rConn = new ReaderConnection();

            string client = form["ddlClient"];
            DateTime sTime = DateTime.Parse(form["lblSDate"]);
            DateTime eTime = DateTime.Parse(form["lblEDate"]);
            string timeZone = form["rdoTimeZone"];

            QueryObj qObj = new QueryObj();

            qObj.school = client;
            qObj.sDate = sTime;
            qObj.eDate = eTime;

            ViewData["QueryObj"] = qObj;

            //check the dates... if start date is newer then end date... just query for the earliest date
            if (sTime > eTime)
            {
                eTime = sTime;
            }

            switch (timeZone)
            {
                case "Pacific":
                    offset = new TimeSpan(-8, 0, 0);
                    break;
                case "Central":
                    offset = new TimeSpan(-6, 0, 0);
                    break;
                case "Eastern":
                    offset = new TimeSpan(-5, 0, 0);
                    break;
                default:
                    offset = new TimeSpan(-7, 0, 0);
                    break;
            }

            ViewBag.StartDate = sTime.ToShortDateString();
            ViewBag.EndDate = eTime.ToShortDateString();

            ViewBag.Date = sTime.ToShortDateString() + " - " + eTime.ToShortDateString();

            //get the offset for the selected times

            sTime = sTime.AddHours(offset.Hours);
            eTime = eTime.AddHours(offset.Hours);

            ViewBag.Client = client;


            //get a list of clients from the DB
            List<string> clients = clientRepo.Clients.Select(x => x.name).ToList();

            if (User.IsInRole("Administration"))
            {
                clients.Add("Demo University");
            }
            else if (User.IsInRole("GH Sales"))
            {
                clients = new List<string>();

                clients.Add("Demo University");
            }

            ViewData["Clients"] = clients;
            List<RecordModel> rModel = new List<RecordModel>();

            switch (client)
            {
                case "Demo University":
                    rModel = rConn.EFGetCallLog("San Juan College", DateTime.Parse("10/27/2017"), DateTime.Parse("10/28/2017"), offset, true);
                    break;
                default:
                    rModel = rConn.EFGetCallLog(client, sTime, eTime, offset, false);
                    break;
            }


            ViewBag.RecordCount = rModel.Count;

            ViewData["Camp"] = rModel.Select(x => x.Campaign).Distinct().ToList();
            ViewData["Dispo"] = rModel.Select(x => x.Disposition).Distinct().ToList();


            TimeSpan threeMin = new TimeSpan(0, 3, 0);
            TimeSpan minThirth = new TimeSpan(0, 1, 30);


            //remove this section after ui design...needs to occur after the user has queried data.


            DailySummaryViewModel dsVM = new DailySummaryViewModel() { AbandonTotal = rModel.Where(x => x.Disposition.ToUpper().Contains("ABANDON")).Count(), AnsUnd90 = rModel.Where(x => new TimeSpan(0, 0, x.QueueTime) < minThirth).Count(), AnsUnd3min = rModel.Where(x => new TimeSpan(0, 0, x.QueueTime) < threeMin).Count(), CallTrans = rModel.Where(x => x.Disposition.ToUpper().Contains("TRANSFER")).Count(), CallTotal = rModel.Count() };

            double abandon = (double)dsVM.AbandonTotal / dsVM.CallTotal;
            double trans = (double)dsVM.CallTrans / dsVM.CallTotal;
            double under3 = (double)dsVM.AnsUnd3min / dsVM.CallTotal;
            double under90 = (double)dsVM.AnsUnd90 / dsVM.CallTotal;

            DailyPercentViewModel dpVM = new DailyPercentViewModel() { AbandonPerc = Math.Round(abandon * 100, 2), TransPrec = Math.Round(trans * 100, 2), Under3min = Math.Round(under3 * 100, 2), Under90 = Math.Round(under90 * 100, 2) };


            List<HourlyCallStatViewModel> hcVM = new List<HourlyCallStatViewModel>();

            string[] times = new string[] { "12:00a", "1:00a", "2:00a", "3:00a", "4:00a", "5:00a", "6:00a", "7:00a", "8:00a", "9:00a", "10:00a", "11:00a", "12:00p", "1:00p", "2:00p", "3:00p", "4:00p", "5:00p", "6:00p", "7:00p", "8:00p", "9:00p", "10:00p", "11:00p" };


            for (int i = 0; i < 24; i++)
            {

                List<RecordModel> templist = rModel.Where(x => x.CallDate.Hour == i).ToList();

                if (templist.Count() > 0)
                {
                    double tempAban = (double)templist.Where(x => x.Disposition.ToUpper().Contains("ABANDON")).Count() / templist.Count();
                    int avghandle = (int)templist.Average(x => x.HandleTime);
                    int avgRing = (int)templist.Average(x => x.RingTime);
                    int avgHold = (int)templist.Average(x => x.HoldTime);
                    int avgQueue = (int)templist.Average(x => x.QueueTime);
                    int avgTalk = (int)templist.Average(x => x.TalkTime);

                    HourlyCallStatViewModel temp = new HourlyCallStatViewModel() { CallHour = times[i], CallTotal = templist.Count(), AbandonPer = Math.Round(tempAban * 100, 2), AvgHandleTime = new TimeSpan(0, 0, avghandle), AvgHoldTime = new TimeSpan(0, 0, avgHold), AvgQueueTime = new TimeSpan(0, 0, avgQueue), AvgRingTime = new TimeSpan(0, 0, avgRing), AvgTalkTime = new TimeSpan(0, 0, avgTalk), TransNum = templist.Where(x => x.Disposition.ToUpper().Contains("TRANSFER")).Count(), AgentCount = templist.Select(x => x.AgentName).Distinct().Count() };

                    hcVM.Add(temp);
                }
            }

            //Create the Pie chart View Model

            List<string> dispos = rModel.Select(x => x.Disposition).Distinct().ToList();
            List<PieChartViewModel> pieChart = new List<PieChartViewModel>();

            foreach (string s in dispos)
            {
                //loop through the list and get the percentage of times it appears
                PieChartViewModel temp = new PieChartViewModel();

                temp.Disposition = s;
                temp.Percentage = Math.Round((double)rModel.Where(x => x.Disposition == s).Count() / rModel.Count() * 100, 2);
                temp.Count = rModel.Where(x => x.Disposition == s).Count();

                pieChart.Add(temp);
            }

            ViewData["DailySummary"] = dsVM;
            ViewData["DailyPerc"] = dpVM;
            ViewData["HourCallStats"] = hcVM;
            ViewData["PieChart"] = pieChart;

            return View("Index", rModel);
        }

        #region Partial Views
        [HttpPost]
        public PartialViewResult GetCallInfo(int callid)
        {
            //get the agent for this call
            five9_call_agent ca = caRepo.CallAgents.FirstOrDefault(x => x.callid == callid);

            if (ca != null)
            {
                ViewBag.Agent = aRepo.Agents.FirstOrDefault(x => x.agentid == ca.agentid).fullname;
            }

            //get the call details
            InteractionDetailsModel idVM = new InteractionDetailsModel();

            five9_call cInfo = callRepo.Calls.FirstOrDefault(x => x.callid == callid);


            if (cInfo != null)
            {
                interaction iInfo = interactionRepo.Interactions.FirstOrDefault(x => x.interactionid == cInfo.interactionid);

                idVM.CallId = callid;
                idVM.CallStart = cInfo.datestart.AddHours(offset.Hours);
                idVM.CallEnd = ((DateTime)cInfo.dateend).AddHours(offset.Hours);
                idVM.Originator = iInfo.originator;
                idVM.Destinator = iInfo.destinator;
                idVM.SessionId = cInfo.sessionid;
                idVM.CallType = itemRepo.Items.FirstOrDefault(x => x.itemid == cInfo.typeid).name;

                ViewData["InteractionDetails"] = idVM;

            }

            //get call stats

            five9_call_time ctInfo = ctRepo.CallTimes.FirstOrDefault(x => x.callid == callid);

            CallTimeViewModel ctVM = new CallTimeViewModel();

            ctVM.Length = new TimeSpan(0, 0, ctInfo.length);
            ctVM.ConferenceTime = new TimeSpan(0, 0, ctInfo.conference_time);
            ctVM.ConsultTime = new TimeSpan(0, 0, ctInfo.consult_time);
            ctVM.HandleTime = new TimeSpan(0, 0, ctInfo.handle_time);
            ctVM.HoldTime = new TimeSpan(0, 0, ctInfo.hold_time);
            ctVM.IVRTime = new TimeSpan(0, 0, ctInfo.ivr_time);
            ctVM.QueueTime = new TimeSpan(0, 0, ctInfo.queue_time);
            ctVM.RingTime = new TimeSpan(0, 0, ctInfo.ring_time);
            ctVM.TalkTime = new TimeSpan(0, 0, ctInfo.talk_time);
            ctVM.Wrapup = new TimeSpan(0, 0, ctInfo.wrapup_time);

            ViewData["CallTime"] = ctVM;

            //get the call segments
            List<five9_call_segment> callSegments = callSegRepo.CallSegments.Where(x => x.callid == callid).ToList();
            List<CallSegmentViewModel> csList = new List<CallSegmentViewModel>();

            foreach (five9_call_segment cs in callSegments)
            {
                CallSegmentViewModel temp = new CallSegmentViewModel()
                {
                    CallType = itemRepo.Items.FirstOrDefault(x => x.itemid == cs.segmenttypeid).name,
                    Result = itemRepo.Items.FirstOrDefault(x => x.itemid == cs.segmentresultid).name,
                    From = cs.calling_party,
                    To = cs.called_party,
                    Duration = new TimeSpan(0, 0, cs.segmenttime)
                };

                csList.Add(temp);
            }

            ViewData["CallSegments"] = csList;

            //get call recordings
            ViewData["CallRecordings"] = recordingRepository.CallRecordings.Where(x => x.callid == callid).ToList();


            return PartialView("_GetCallInfo");
        }

        [HttpPost]
        public PartialViewResult GetCallInfoDemo()
        {
            return PartialView("_GetCallInfoDemo");
        }

        public PartialViewResult GetTrendsData(QueryObj qStats)
        {
            //get 2 weeks worth of call counts to do a weekly comparison... 14 days after end date
            DateTime queryDate = qStats.eDate.AddDays(-14);
            ReaderConnection rc = new ReaderConnection();

            DateTime qDate = DateTime.Parse(queryDate.ToShortDateString());
            DateTime endDate = DateTime.Parse(qStats.eDate.ToShortDateString());

            List<CallVolumeDate> cvd = rc.GetCallVolumeforDates(qDate, endDate, qStats.school);

            //get a string of unique days
            List<string> dates = cvd.OrderBy(e => e.Date).Select(x => x.Date.ToShortDateString()).Distinct().ToList();

            List<CallVolumeDate> cvdModel = new List<CallVolumeDate>();

            //loop through and get the count for calls on this date
            foreach (string s in dates)
            {
                CallVolumeDate tempModel = new CallVolumeDate();

                tempModel.Date = DateTime.Parse(s);
                tempModel.CallCount = cvd.FindAll(x => x.Date.ToShortDateString() == s).Count();
                tempModel.DayOfWeek = DateTime.Parse(s).DayOfWeek.ToString();

                cvdModel.Add(tempModel);
            }

            List<CallVolumeDate> fWeek = new List<CallVolumeDate>();
            List<CallVolumeDate> sWeek = new List<CallVolumeDate>();

            int weekOne = 0;
            int weekTwo = 0;

            for (int i = 0; i < cvdModel.Count(); i++)
            {
                if (i < 7)
                {
                    //first week bracket
                    weekOne += cvdModel[i].CallCount;
                    fWeek.Add(new CallVolumeDate() { DayOfWeek = cvdModel[i].Date.DayOfWeek.ToString(), CallCount = cvdModel[i].CallCount });
                }
                else
                {
                    weekTwo += cvdModel[i].CallCount;
                    sWeek.Add(new CallVolumeDate() { DayOfWeek = cvdModel[i].Date.DayOfWeek.ToString(), CallCount = cvdModel[i].CallCount });
                }

            }

            //get rid of saturday and sunday
            cvdModel = cvdModel.Where(x => x.DayOfWeek != "Saturday" && x.DayOfWeek != "Sunday").ToList();
            fWeek = fWeek.Where(x => x.DayOfWeek != "Saturday" && x.DayOfWeek != "Sunday").ToList();
            sWeek = sWeek.Where(x => x.DayOfWeek != "Saturday" && x.DayOfWeek != "Sunday").ToList();

            ViewData["CallDef"] = weekTwo - weekOne;
            ViewData["Avg"] = cvdModel.Average(x => x.CallCount);

            ViewData["Max"] = cvdModel.OrderByDescending(x => x.CallCount).First();
            ViewData["Min"] = cvdModel.OrderBy(x => x.CallCount).First();

            double avg = cvdModel.Average(x => x.CallCount);

            ViewData["WeekOneStats"] = fWeek;
            ViewData["WeekTwoStats"] = sWeek;

            return PartialView("_GetTrendsData");
        }
        #endregion
    }
}
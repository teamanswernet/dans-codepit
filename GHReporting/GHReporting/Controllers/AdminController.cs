﻿using GHReporting.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using OpsLibrary;
using OpsLibrary.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace GHReporting.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private RoleManager<IdentityRole> roleManager;

        public AdminController()
        {
        }

        public AdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {

            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Admin
        public ActionResult Index(string msg)
        {
            //get all the users that are in the system with their role
            List<UserViewModel> userList = new List<UserViewModel>();
            var users = UserManager.Users.ToList();

            foreach (ApplicationUser au in users)
            {
                UserViewModel temp = new UserViewModel();

                temp.UserId = au.Id;
                temp.Username = au.UserName;
                temp.Email = au.Email;


                if (au.Roles.Count > 0)
                {
                    temp.Role = UserManager.GetRoles(au.Id).FirstOrDefault();
                }
                else
                {
                    temp.Role = "";
                }

                userList.Add(temp);
            }


            List<string> roles = new List<string>();

            roles.Add("Administrator");
            roles.Add("GH Sales");

            //get a list of the schools
            using (var context = new EFDataExchange())
            {
                List<client> clients = context.clients.ToList();

                foreach (client c in clients)
                {
                    roles.Add(c.name);
                }
            }

            ViewData["Roles"] = roles;

            if(msg != null)
            {
                ViewBag.Result = msg;
            }

            return View(userList.OrderBy(x => x.Username));
        }

        public ActionResult UpdateUser(FormCollection form)
        {
            string email = form["Email"];
            string phoneNum = form["PhoneNumber"];
            string id = form["Id"];
            string role = form["ddlRole"];

            using (var context = new GHOperations())
            {
                AspNetUser user = context.AspNetUsers.FirstOrDefault(x => x.Id == id);

                if (user != null)
                {
                    user.Email = email;
                    user.PhoneNumber = phoneNum;

                    context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                }

            }

            //see if user has a role
            var roles = UserManager.GetRoles(id);
            roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            //check to see if the role is already in the system
            if (!roleManager.RoleExists(role))
            {
                var r = new IdentityRole();
                r.Name = role;
                roleManager.Create(r);
            }


            if (roles.Count > 0)
            {
                //if theirs a claim for this user in the system
                foreach (string c in roles)
                {
                    UserManager.RemoveFromRole(id, c);
                }

                //add claim with this user for selected role
                UserManager.AddToRole(id, role);
            }
            else
            {
                //add this user for selected role
                UserManager.AddToRole(id, role);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public PartialViewResult GetUserInfo(string userId)
        {
            AspNetUser user = new AspNetUser();

            List<string> roles = new List<string>();

            roles.Add("Administrator");
            roles.Add("GH Sales");
            
            try
            {
                using (var context = new GHOperations())
                {
                    user = context.AspNetUsers.FirstOrDefault(x => x.Id == userId);


                }
                //get a list of the schools
                using (var context = new EFDataExchange())
                {
                    List<client> clients = context.clients.ToList();

                    foreach (client c in clients)
                    {
                        roles.Add(c.name);
                    }
                }
            }
            catch (Exception)
            {

            }

            ViewBag.Userid = user.Id;
            ViewData["Roles"] = roles;

            return PartialView("_GetUserInfo", user);
        }

        [HttpPost]
        //[Authorize(Users = "dwright@greenwoodhall.com")]
        [ValidateAntiForgeryToken]
        public ActionResult Register(FormCollection form)
        {
            string message = "";
            string email = form["txtEmail"];
            string password = form["txtPassword"];

            var user = new ApplicationUser { UserName = email.Trim(), Email = email.Trim() };
            var result = UserManager.Create(user, password);

            if (result.Succeeded)
            {
                ApplicationUser us = UserManager.Users.FirstOrDefault(x => x.UserName == email);

                string role = form["ddlRole"];

                roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

                //check to see if the role is already in the system
                if (!roleManager.RoleExists(role))
                {
                    var r = new IdentityRole();
                    r.Name = role;
                    roleManager.Create(r);
                }

                //add this user for selected role
                UserManager.AddToRole(us.Id, role);


                message = "User has been Created!";
            }
            else
            {
                message = result.Errors.FirstOrDefault();
            }


            // If we got this far, something failed, redisplay form
            return RedirectToAction("Index", new { msg = message });
        }

        public ActionResult RemoveUser(string userId)
        {

            try
            {
                using (var context = new GHOperations())
                {
                    AspNetUser user = context.AspNetUsers.FirstOrDefault(x => x.Id == userId);

                    //TODO: Be sure to clear out of Role/Claim Tables when that part of system is put together

                    if (user != null)
                    {
                        context.Entry(user).State = System.Data.Entity.EntityState.Deleted;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {

            }

            return RedirectToAction("Index");
        }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GHReporting.Startup))]
namespace GHReporting
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

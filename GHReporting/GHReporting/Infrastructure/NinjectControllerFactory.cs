﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using OpsLibrary.Abstract;
using OpsLibrary.Concrete;

namespace GHReporting.Infrastructure
{
    public class NinjectControllerFactory: DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            //add bindings here
            ninjectKernel.Bind<iClientRepository>().To<EFClientRepository>();
            ninjectKernel.Bind<iCallTimeRepository>().To<EFCallTimeRepository>();
            ninjectKernel.Bind<iCallAgentRepository>().To<EFCallAgentRepository>();
            ninjectKernel.Bind<iAgentRepository>().To<EFAgentRepository>();
            ninjectKernel.Bind<iCallRepository>().To<EFCallRepository>();
            ninjectKernel.Bind<iInteractionRepository>().To<EFInteractions>();
            ninjectKernel.Bind<iItemRepository>().To<EFItemRepository>();
            ninjectKernel.Bind<iCallSegmentRepository>().To<EFCallSegementRepository>();
            ninjectKernel.Bind<iCallRecordingRepository>().To<EFCallRecordingRepository>();
        }
    }//end class
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="hgtc.aspx.cs" Inherits="hgtc" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>HGTC Request Information Web Form</title>
        <%--<link rel="shortcut icon" type="image/x-icon" href="https://ci53.actonsoftware.com/acton/image/favicon.png" />
        <link rel="icon" type="image/x-icon" href="https://ci53.actonsoftware.com/acton/image/favicon.png" />--%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="og:title" content="HGTC Request Information Web Form" />
        <meta http-equiv="expires" value="Sat, 1 Jan 2000 11:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
        <script type="text/javascript" src="Scripts/jquery-2.1.0.min.js"></script>
        <script type="text/javascript" src="Scripts/jquery.maskedinput-1.3.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <script type="text/javascript">
            var r = 'l-0012:246';
            var doPrefill = 0;
            function doPrefillOnloadDone(ajax) { }
            function doPrefillOnload() {
                if (r && r.length > 0 && doPrefill) {
                    var parms = 'a=8952&id=0002&r=' + r + '&did=0002:d-0001&ts=' + (new Date().getTime());
                    new Ajax.Updater(
                        'prefillDiv',
                        '/acton/forms/prefillAjax.jsp',
                        {
                            method: 'get',
                            parameters: parms,
                            evalScripts: true,
                            onComplete: doPrefillOnloadDone
                        }
                        );
                }
            }
            function doOnLoad() {
                document.getElementById("ao_jstzo").value = new Date().getTimezoneOffset();
                var refURL = document.referrer;
                if (window.location.href.indexOf('embedded=1') !== -1) {
                    try {
                        var parentReferrer = parent.getReferrerHeader();
                        if (!parentReferrer || parentReferrer == '')
                            refURL = getParameter("refUrl");
                        else refURL = parentReferrer;
                    }
                    catch (e) {
                        refURL = getParameter("refUrl");
                    }
                }
                if (refURL != null)
                    document.getElementById("ao_refurl").value = refURL;
                doPrefillOnload();
            
                $("#form_0002_fld_0_fn").attr("name", "First Name");
                $("#form_0002_fld_0_ln").attr("name", "Last Name");
                $("#form_0002_fld_0_em").attr("name", "E-mail Address");
                $("#form_0002_fld_11").attr("name", "Home Phone");
                $("#form_0002_fld_12").attr("name", "Cell Phone");
                $("#form_0002_fld_2_Street").attr("name", "Home Street");
                $("#form_0002_fld_2_City").attr("name", "Home City");
                $("#form_0002_fld_2_State").attr("name", "Home State");
                $("#form_0002_fld_2_Zip").attr("name", "Home Postal Code");
                $("#form_0002_fld_2_Country").attr("name", "Home Country");
                $("#form_0002_fld_3").attr("name", "High School Graduation Date");
                $("#form_0002_fld_4").attr("name", "High School Name");
                $("#form_0002_fld_5").attr("name", "Major Area Interested In");
                $("#form_0002_fld_6").attr("name", "Start Date");
                $("#form_0002_fld_7").attr("name", "Additional Comments");
            }
            
            function openWindow(url) {
                window.open(url, "popup");
            }
            
            function ValidateInputs() {
            
            
                Page_ClientValidate();//Validate the validators using microsofts validation
            
                var validators = Page_Validators;
                var isValid = true;

                for (var i = 0; i < validators.length; i++) {
                    var validator = validators[i];
                    if (!validator.isvalid) {
                        $("#" + validator.controltovalidate).addClass("validation-error");
                        //document.getElementById(validator.controltovalidate).setAttribute("style", "border:solid 1px red;");
                        isValid = false;
                    }
                    else {
                        $("#" + validator.controltovalidate).removeClass("validation-error");
                    }
                }

                // The form on the iFrame does not behave the same as when directly visited
                // For this reason we disable the button and hide the form on a validated submit
                // This is a temporary fix since we need to ensure the form was actually submitted as well

                if (isValid) {
                    // $("#form_0002_ao_submit_input").attr("disabled", "disabled");
                    // $("#form_0002_ao_submit_input").val("Submitted");
                    // $("#form_0002_ao_submit_input").css("background-color", "#8cb6de")

                    $(".ao_tbl_container").hide();
                    $(".ao_tbl_submitted").show();
                }
            }
            
            $(function () {
                $("#form_0002_fld_11").mask("(999) 999-9999");
                $("#form_0002_fld_12").mask("(999) 999-9999");
                $("#form_0002_fld_2_Zip").mask("99999");
                $("#form_0002_fld_6").val("Summer 2017");
                $("#form_0002_fld_2_State").val("SC");
            
                $("#form_0002_ao_submit_input").click(function (e) {
            
                    //var mm = $("#form_0002_fld_3_MM").val();
                    //var dd = $("#form_0002_fld_3_dd").val();
                    //var yy = $("#form_0002_fld_3_yyyy").val();
            
                    ValidateInputs();
            
                    var mm = parseInt($("#form_0002_fld_3_MM").val(), 10);
                    //var dd = parseInt($("#form_0002_fld_3_dd").val(), 10);
                    var yy = parseInt($("#form_0002_fld_3_yyyy").val(), 10);
            
                    //var date = new Date(yy, mm - 1, dd);
                    //if (date.getFullYear() == yy && date.getMonth() + 1 == mm && date.getDate() == dd) {
            
                        //$("#form_0002_fld_3").val($("#form_0002_fld_3_MM").val() + "/" + $("#form_0002_fld_3_dd").val() + "/" + $("#form_0002_fld_3_yyyy").val());
                        $("#form_0002_fld_3").val($("#form_0002_fld_3_MM").val() + "/" + $("#form_0002_fld_3_yyyy").val());
            
                    //} else {
                        //alert('Invalid date');
                    //}
            
                    $("#ao_bot").val("nope");
            
            
                    //e.preventDefault();
                    //e.stopPropagation();
                    //return false;
                });
            
            
            });
        </script>
        <!-- Google Tag Manager -->
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-MJDZNPH');</script>
        <!-- End Google Tag Manager -->
    </head>
    <body onload="doOnLoad()">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MJDZNPH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <form id="form_0002" runat="server">
            <input type="hidden" name="ao_a" value="8952">
            <input type="hidden" name="ao_f" value="0002">
            <input type="hidden" name="ao_d" value="0002:d-0001">
            <input type="hidden" name="ao_p" id="ao_p" value="0">
            <input type="hidden" name="ao_jstzo" id="ao_jstzo" value="">
            <input type="hidden" name="ao_refurl" id="ao_refurl" value="">
            <input type="hidden" name="ao_cuid" value="l-0012:246">
            <input type="hidden" name="ao_srcid" value="">
            <input type="hidden" name="ao_nc" value="">
            <input type="hidden" name="ao_pf" value="0">
            <input type="hidden" name="ao_bot" id="ao_bot" value="">
            <input type="hidden" name="ao_camp" value="">
            <input type="hidden" name="ao_campid" value="">
            <!-- -------------------------------------------------------------------------------------------- -->
            <style type="text/css">
                table, tr, td, body, p, div, span, textarea, input, select, a, ul, ol {
                    font-size: 11pt;
                }
            </style>
            <!-- -------------------------------------------------------------------------------------------- -->
            <div id="ao_alignment_container" align="center">
                <table class="ao_tbl_container" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="formFieldLabel">First Name
                                                <b style="color: #FF0000; cursor: default" title="Required Field">
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_0_fn" Display="Dynamic" EnableClientScript="true" ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b></td>
                                            <td class="formFieldLabel" style="padding-left: 5px">Last Name
                                                <b style="color: #FF0000; cursor: default" title="Required Field">
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_0_ln" Display="Dynamic" EnableClientScript="true" ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                                </b></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" class="l6e formFieldText formFieldMediumLeft" id="form_0002_fld_0_fn" name="First Name" value="" runat="server">


                                            </td>
                                            <td style="padding-left: 5px">
                                                <input type="text" class="l6e formFieldText formFieldMediumRight" id="form_0002_fld_0_ln" runat="server" name="Last Name" value="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="formFieldLabel" colspan="2">Email
                                                <b style="color: #FF0000; cursor: default" title="Required Field">
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_0_em" EnableClientScript="true" Display="Dynamic" ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator runat="server" ID="revEmail" Display="Dynamic" ControlToValidate="form_0002_fld_0_em" EnableClientScript="true" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">* Invalid email format</asp:RegularExpressionValidator>
                                                    <%-- ^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$ --%>
                                                    <%--
                                                        OnServerValidate="cvEmail_ServerValidate"
                                                    --%>
                                                    <asp:CustomValidator ID="cvEmail" runat="server" ControlToValidate="form_0002_fld_0_em" Display="Dynamic" ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true" ClientValidationFunction="isEmailValid">* Email does not exist!</asp:CustomValidator>
                                                </b>
                                                <asp:HiddenField ID="emailValidated" runat="server" />
                                                <asp:HiddenField ID="emailValidatedResult" runat="server" />
                                                <script type="text/javascript">
                                                    function isEmailValid(source, arguments) {
                                                        // This submits the Email Validator
                                                        // This patch makes it so we do not validate the email if it's not a valid email address
                                                        // 
                                                        // I also implemented 2 hidden fields to not validate the same email address multiple times
                                                        // This won't help if the user refreshes, but should stop it from using excessive credits

                                                        var isValid;

                                                        var revEmail = document.getElementById("<%=revEmail.ClientID%>");
                                                        ValidatorValidate(revEmail);

                                                        if (revEmail.isvalid) {

                                                            var emailToValidate = arguments.Value;
                                                            var emailValidated = document.getElementById("<%=emailValidated.ClientID%>");
                                                            var emailValidatedResult = document.getElementById("<%=emailValidatedResult.ClientID%>");
                                                            var valType = "";

                                                            var doEmailValidator = true; // Change to false if we want to bypass the PAID email validator
                                                            if (doEmailValidator && (emailValidated.value != emailToValidate)) {
                                                                $.ajax({
                                                                    type: "POST",
                                                                    contentType: "application/json; charset=utf-8",
                                                                    url: "EmailValidatorDotNetHandler.ashx?email=" + arguments.Value,
                                                                    data: "{'email': '" + arguments.Value + "'}",
                                                                    dataType: "json",
                                                                    async: false,
                                                                    success: function (data) {
                                                                        isValid = data.verify_status;
                                                                    }
                                                                });
                                                                valType = "API";
                                                            } else {
                                                                if (!doEmailValidator) { emailValidatedResult.value = "true"; } // If the PAID email validator is bypassed, we ensure no errors are caught

                                                                if (emailValidatedResult.value == "true") { isValid = true; } else { isValid = false; }
                                                                valType = "SES";
                                                            }

                                                            emailValidated.value = emailToValidate;
                                                            emailValidatedResult.value = isValid;

                                                            // DeBug
                                                            // alert(emailToValidate + "\n" + emailValidated.value + "\n" + emailValidatedResult.value + "\n" + valType);

                                                        } else {
                                                            isValid = true;
                                                        }


                                                        arguments.IsValid = isValid;
                                                    }
                                                    
                                                </script>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="2">
                                                <input type="Email" class="l6e formFieldText formFieldLarge" id="form_0002_fld_0_em" runat="server" name="E-mail Address" value="" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="formFieldLabel">Home Phone
                                                <b style="color: #FF0000; cursor: default" title="Required Field">
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_11" Display="Dynamic" EnableClientScript="true" ValidationGroup="hgtc" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                                </b></td>
                                            <td class="formFieldLabel" style="padding-left: 5px">Cell Phone
                                                <b style="color: #FF0000; cursor: default" title="Required Field">
                                                    <%--
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0004_fld_12" Display="Dynamic" EnableClientScript="true" 
                                                                                                       ValidationGroup="hgtc" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                                    --%>
                                                </b></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" class="l6e formFieldText formFieldMediumLeft" id="form_0002_fld_11" runat="server" name="Home Phone" value="">
                                            </td>
                                            <td style="padding-left: 5px">
                                                <input type="text" class="l6e formFieldText formFieldMediumRight" id="form_0002_fld_12" runat="server" name="Cell Phone" value="">
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <table cellspacing="0" cellpadding="0">
                                        <!-- Street -->
                                        <tr>
                                            <td colspan="2" class="formFieldLabel">Mailing Address
                                                <b style="color: #FF0000; cursor: default" title="Required Field">
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_2_Street" Display="Dynamic" EnableClientScript="true" ValidationGroup="hgtc" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                                </b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <input type="text" class="formFieldText formFieldLarge" id="form_0002_fld_2_Street" runat="server" name="Home Street" value="">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <!-- City, State -->
                                        <tr>
                                            <td class="formFieldLabel">City
                                                <b style="color: #FF0000; cursor: default" title="Required Field">
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_2_City" Display="Dynamic" EnableClientScript="true" ValidationGroup="hgtc" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                                </b></td>
                                            <td class="formFieldLabel" style="padding-left: 5px">State
                                                <b style="color: #FF0000; cursor: default" title="Required Field">
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_2_State" Display="Dynamic" EnableClientScript="true" ValidationGroup="hgtc" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                                </b></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" class="formFieldText formFieldMediumLeft" id="form_0002_fld_2_City" runat="server" name="Home City" value="">
                                            </td>
                                            <td style="padding-left: 5px">

                                                <select class="formFieldMediumRight" name="Home State" id="form_0002_fld_2_State" runat="server">
                                                    <option value="">---Select---</option>
                                                    <option value="AL">Alabama</option>
                                                    <option value="AK">Alaska</option>
                                                    <option value="AZ">Arizona</option>
                                                    <option value="AR">Arkansas</option>
                                                    <option value="CA">California</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="CT">Connecticut</option>
                                                    <option value="DE">Delaware</option>
                                                    <option value="DC">District of Columbia</option>
                                                    <option value="FL">Florida</option>
                                                    <option value="GA">Georgia</option>
                                                    <option value="HI">Hawaii</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="IL">Illinois</option>
                                                    <option value="IN">Indiana</option>
                                                    <option value="IA">Iowa</option>
                                                    <option value="KS">Kansas</option>
                                                    <option value="KY">Kentucky</option>
                                                    <option value="LA">Louisiana</option>
                                                    <option value="ME">Maine</option>
                                                    <option value="MD">Maryland</option>
                                                    <option value="MA">Massachusetts</option>
                                                    <option value="MI">Michigan</option>
                                                    <option value="MN">Minnesota</option>
                                                    <option value="MS">Mississippi</option>
                                                    <option value="MO">Missouri</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NV">Nevada</option>
                                                    <option value="NH">New Hampshire</option>
                                                    <option value="NJ">New Jersey</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="NY">New York</option>
                                                    <option value="NC">North Carolina</option>
                                                    <option value="ND">North Dakota</option>
                                                    <option value="OH">Ohio</option>
                                                    <option value="OK">Oklahoma</option>
                                                    <option value="OR">Oregon</option>
                                                    <option value="PA">Pennsylvania</option>
                                                    <option value="PR">Puerto Rico</option>
                                                    <option value="RI">Rhode Island</option>
                                                    <option value="SC">South Carolina</option>
                                                    <option value="SD">South Dakota</option>
                                                    <option value="TN">Tennessee</option>
                                                    <option value="TX">Texas</option>
                                                    <option value="UT">Utah</option>
                                                    <option value="VT">Vermont</option>
                                                    <option value="VA">Virginia</option>
                                                    <option value="WA">Washington</option>
                                                    <option value="WV">West Virginia</option>
                                                    <option value="WI">Wisconsin</option>
                                                    <option value="WY">Wyoming</option>
                                                    <option value="AB">Alberta</option>
                                                    <option value="BC">British Columbia</option>
                                                    <option value="MB">Manitoba</option>
                                                    <option value="NB">New Brunswick</option>
                                                    <option value="NL">Newfoundland and Labrador</option>
                                                    <option value="NS">Nova Scotia</option>
                                                    <option value="ON">Ontario</option>
                                                    <option value="PE">Prince Edward Island</option>
                                                    <option value="QC">Quebec</option>
                                                    <option value="SK">Saskatchewan</option>
                                                    <option value="INT">International/Other</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <!-- Zip, Country -->
                                        <tr>
                                            <td class="formFieldLabel" colspan="2">Zip Code
                                                <b style="color: #FF0000; cursor: default" title="Required Field">
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_2_Zip" Display="Dynamic" EnableClientScript="true" ValidationGroup="hgtc" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                                </b></td>

                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <input type="text" class="formFieldText formFieldMediumLeft" id="form_0002_fld_2_Zip" runat="server" name="Home Postal Code" value="">

                                                <select id="form_0002_fld_2_Country" name="Home Country" class="formFieldMediumRight" style="display:none;">
                                                    <option value=""></option>

                                                </select>
                                            </td>
                                        </tr>


                                    </table>
                                </div>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <div class="formFieldLabel" id="form_0002_fld_3-Label">High School Graduation Date</div>
                                    <input type="hidden" id="form_0002_fld_3" name="High School Graduation Date" value="">
                                    <select id="form_0002_fld_3_MM">
                                        <option value="" selected="selected">MM</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                    <%--
                                        <select id="form_0002_fld_3_dd"">
                                                                            <option value="" selected="selected">DD</option>
                                                                            <option value="01">01</option>
                                                                            <option value="02">02</option>
                                                                            <option value="03">03</option>
                                                                            <option value="04">04</option>
                                                                            <option value="05">05</option>
                                                                            <option value="06">06</option>
                                                                            <option value="07">07</option>
                                                                            <option value="08">08</option>
                                                                            <option value="09">09</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            <option value="15">15</option>
                                                                            <option value="16">16</option>
                                                                            <option value="17">17</option>
                                                                            <option value="18">18</option>
                                                                            <option value="19">19</option>
                                                                            <option value="20">20</option>
                                                                            <option value="21">21</option>
                                                                            <option value="22">22</option>
                                                                            <option value="23">23</option>
                                                                            <option value="24">24</option>
                                                                            <option value="25">25</option>
                                                                            <option value="26">26</option>
                                                                            <option value="27">27</option>
                                                                            <option value="28">28</option>
                                                                            <option value="29">29</option>
                                                                            <option value="30">30</option>
                                                                            <option value="31">31</option>
                                                                        </select>
                                    --%>
                                    <select id="form_0002_fld_3_yyyy">
                                        <option value="" selected="selected">YYYY</option>
                                        <option value="1950">1950</option>
                                        <option value="1951">1951</option>
                                        <option value="1952">1952</option>
                                        <option value="1953">1953</option>
                                        <option value="1954">1954</option>
                                        <option value="1955">1955</option>
                                        <option value="1956">1956</option>
                                        <option value="1957">1957</option>
                                        <option value="1958">1958</option>
                                        <option value="1959">1959</option>
                                        <option value="1960">1960</option>
                                        <option value="1961">1961</option>
                                        <option value="1962">1962</option>
                                        <option value="1963">1963</option>
                                        <option value="1964">1964</option>
                                        <option value="1965">1965</option>
                                        <option value="1966">1966</option>
                                        <option value="1967">1967</option>
                                        <option value="1968">1968</option>
                                        <option value="1969">1969</option>
                                        <option value="1970">1970</option>
                                        <option value="1971">1971</option>
                                        <option value="1972">1972</option>
                                        <option value="1973">1973</option>
                                        <option value="1974">1974</option>
                                        <option value="1975">1975</option>
                                        <option value="1976">1976</option>
                                        <option value="1977">1977</option>
                                        <option value="1978">1978</option>
                                        <option value="1979">1979</option>
                                        <option value="1980">1980</option>
                                        <option value="1981">1981</option>
                                        <option value="1982">1982</option>
                                        <option value="1983">1983</option>
                                        <option value="1984">1984</option>
                                        <option value="1985">1985</option>
                                        <option value="1986">1986</option>
                                        <option value="1987">1987</option>
                                        <option value="1988">1988</option>
                                        <option value="1989">1989</option>
                                        <option value="1990">1990</option>
                                        <option value="1991">1991</option>
                                        <option value="1992">1992</option>
                                        <option value="1993">1993</option>
                                        <option value="1994">1994</option>
                                        <option value="1995">1995</option>
                                        <option value="1996">1996</option>
                                        <option value="1997">1997</option>
                                        <option value="1998">1998</option>
                                        <option value="1999">1999</option>
                                        <option value="2000">2000</option>
                                        <option value="2001">2001</option>
                                        <option value="2002">2002</option>
                                        <option value="2003">2003</option>
                                        <option value="2004">2004</option>
                                        <option value="2005">2005</option>
                                        <option value="2006">2006</option>
                                        <option value="2007">2007</option>
                                        <option value="2008">2008</option>
                                        <option value="2009">2009</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2015">2016</option>
                                        <option value="2015">2017</option>
                                        <option value="2015">2018</option>
                                    </select>
                                    <input type="hidden" id="form_0002_fld_3_pattern" name="form_0002_fld_3_pattern" value="MM/dd/yyyy">
                                </div>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <div class="formFieldLabel">High School Name</div>
                                    <input type="text" class="formFieldText formFieldMedium" id="form_0002_fld_4" runat="server" name="High School Name">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <div class="formFieldLabel">Major Area Interested In
                                        <b style="color: #FF0000; cursor: default" title="Required Field">
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_5" Display="Dynamic" EnableClientScript="true" ValidationGroup="hgtc" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                        </b></div>
                                    <select size="1" id="form_0002_fld_5" runat="server" name="Major Area Interested In" class="">
                                        <option value="">Choose area of interest</option>
                                        <option value="Academic">Undecided</option>
                                        <option value="Health Sciences">Health Sciences</option>
                                        <option value="Arts,Sciences,Transfer">Arts, Sciences, Transfer</option>
                                        <option value="Engineering">Engineering</option>
                                        <option value="Information Technology/Digital Arts">Information Tech/Digital Arts</option>
                                        <option value="Manufacturing Technology">Manufacturing and Industrial Technologies</option>
                                        <option value="Natural Resources">Natural Resources</option>
                                        <option value="Occupational Technology">Occupational Technology</option>
                                        <option value="Personal Care/Related Health">Personal Care/Related Health</option>
                                        <option value="Public Service Technology">Public Service Technology</option>
                                        <option value="Business Administration">Business Administration</option>
                                        <option value="Culinary Arts">Culinary Arts</option>
                                    </select>

                                    <a class="link" href="javascript:void(0)" onclick="javascript:openWindow('https://www.hgtc.edu/academics/academic_departments/')">See all programs</a>
                                </div>

                            </div>
                        </td>
                    </tr>
                    <%--
                        <tr>
                                            <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                                                <div align="left">
                                                    <div class="formField">
                                                        Click here to see all majors:<br /><a class="link" href="javascript:void(0)" onclick="javascript:openWindow('https://www.hgtc.edu/academics/academic_departments/')">https://www.hgtc.edu/academics/academic_departments/</a>
                                                        <br />
                                                        <br />
                                                     </div>
                                                </div>
                                            </td>
                                        </tr>
                    --%>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <div class="formFieldLabel">Semester Start Date
                                        <b style="color: #FF0000; cursor: default" title="Required Field">
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0002_fld_6" Display="Dynamic" EnableClientScript="true" ValidationGroup="hgtc">*</asp:RequiredFieldValidator>
                                        </b></div>
                                    <select size="1" id="form_0002_fld_6" runat="server" name="Start Date" class="">
                                        <option value=""></option>                                     
                                        <option value="Spring 2018">Spring 2018</option>
                                        <option value="Summer 2018">Summer 2018</option>
                                        <option value="Fall 2018">Fall 2018</option>
                                    </select>
                                </div>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <div class="formFieldLabel">Additional Comments</div>
                                    <textarea class="formTextArea formTextAreaSmall formTextAreaWidthLarge" id="form_0002_fld_7" runat="server" name="Additional Comments"></textarea>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" id="form_0002_ao_submit_button">
                                                <%--<input id="form_0002_ao_submit_input" type="button" name="Submit" value="Submit" runat="server">--%>
                                                <asp:Button ID="form_0002_ao_submit_input" runat="server" CssClass="submit" CausesValidation="true" ValidationGroup="hgtc" Text="Submit" UseSubmitBehavior="true" />


                                                <%--
                                                    https://ci53.actonsoftware.com/acton/eform/8952/0004/d-ext-0001 
                                                    
                                                                                                   PostBackUrl="https://ci53.actonsoftware.com/acton/eform/8952/0004/d-ext-0001"
                                                --%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="formNegCap">
                        <td>
                            <input type="text" id="ao_form_neg_cap" name="ao_form_neg_cap" value="">
                        </td>
                    </tr>
                </table>
                <table class="ao_tbl_submitted" border="0" cellspacing="0" cellpadding="0" style="display: none;">
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="left" colspan="2">
                            <h3>Thank you, your information has been submitted.</h3>
                        </td>
                    </tr>
                </table>
            </div>

        </form>
        <div id="prefillDiv" style="display: none"></div>


        <script type="text/javascript">

            
            try{
            
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            
            ga('create', 'UA-5012196-1', 'auto', {
            
              'allowLinker': true
            
            });
            
            ga('set', {
            
              page: '/requestinfo-confirmation',
            
              title: 'Request Info Confirmation'
            
            });
            
            ga('send', 'pageview');
            
            }catch (err) {}
            
        </script>

        <script type="text/javascript">
            /* <![CDATA[ */
            document.write(
              '<img src="https://ci53.actonsoftware.com/acton/bn/8952/visitor.gif?ts=' +
              new Date().getTime() +
              '&ref=' + escape(document.referrer) + '">'
            );
            var aoAccountId = '8952';
            var aoCookieMode = 'STANDARD';
            var aoCookieDomain = 'actonsoftware.com';
            var aoServerContext = 'https://ci53.actonsoftware.com/acton';
            /* ]]> */
        </script>


    </body>
</html>

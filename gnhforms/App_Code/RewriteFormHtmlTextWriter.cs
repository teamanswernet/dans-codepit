﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for RewriteFormHtmlTextWriter
/// </summary>
public class FormRewriterControlAdapter : System.Web.UI.Adapters.ControlAdapter
{
    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(new RewriteFormHtmlTextWriter(writer));
    }
}
public class RewriteFormHtmlTextWriter : HtmlTextWriter
{
    public RewriteFormHtmlTextWriter(HtmlTextWriter writer)
        : base(writer)
    {
        this.InnerWriter = writer.InnerWriter;
    }
    public RewriteFormHtmlTextWriter(System.IO.TextWriter writer)
        : base(writer)
    {
        base.InnerWriter = writer;
    }

    public override void WriteAttribute(string name, string value, bool fEncode)
    {
        if (name == "action")
        {
            HttpContext context = HttpContext.Current;

            string page = Path.GetFileName(context.Request.Url.AbsolutePath).ToLower();
            switch(page)
            {
                case "hgtc.aspx":
                    value = ConfigurationManager.AppSettings["HGTC_Form"];
                    break;
                case "campus_tours.aspx":
                    value = ConfigurationManager.AppSettings["CampusTours_Form"];
                    break;
                case "campus_tours_new.aspx":
                    value = ConfigurationManager.AppSettings["CampusTours_Form"];
                    break;
                default:
                    value = ConfigurationManager.AppSettings["HGTC_Form"];
                    break;
            }


            //value = "http://ci53.actonsoftware.com/acton/forms/userSubmit.jsp";
            //value = "hgtc_v2.aspx";
        }

        base.WriteAttribute(name, value, fEncode);
    }
}
﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;

public class APIResult
{
    public int status { get; set; }
    public String info { get; set; }
    public String details { get; set; }
}

public class EmailValidatorDotNetHandler : IHttpHandler
{

    public EmailValidatorDotNetHandler()
    {

    }

    public void ProcessRequest(HttpContext context)
    {
        HttpRequest req = context.Request;
        HttpResponse res = context.Response;

        //string APIURL  = "http://www.email-validator.net/api/verify";
        string APIURL = "http://api.email-validator.net/api/verify";
        //string apiKey = "ev-3efbf8d4f5a35a29d90741f0f2327665"; // MIS - monthly
        //string apiKey = "ev-e5cad172096ddb1ee451e1e38e36edf9"; // temp 11/4/2014
        //string apiKey = "ev-a3bbac2871b261bbe636d61f2de1bebf"; // 9/9/2015
        string apiKey = "ev-896fd1165164a2de048a8e2b1d549167";

        string email = req.QueryString["email"];
        res.ContentType = "application/json";

        Result returnData = new Result();

        if (!String.IsNullOrEmpty(email))
        {
            HttpClient client = new HttpClient();

            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("EmailAddress", email));
            postData.Add(new KeyValuePair<string, string>("APIKey", apiKey));

            HttpContent content = new FormUrlEncodedContent(postData);

            HttpResponseMessage result = client.PostAsync(APIURL, content).Result;
            string resultContent = result.Content.ReadAsStringAsync().Result;

            APIResult data = new System.Web.Script.Serialization.JavaScriptSerializer().
                                Deserialize<APIResult>(resultContent);

            returnData.verify_output = data.status.ToString();
            returnData.verify_status_desc = data.details;
            //returnData.verify_status_desc = postData.toString();
            returnData.limit_status = data.info;
            //returnData.origination = postData.toString();

            switch (data.status)
            {
                // valid addresses have a {200, 207, 215} result code
                // result codes 114 and 118 need a retry
                case 200:
                case 207:
                case 215:
                    returnData.verify_status = true;

                    // Address is valid
                    break;
                case 114:
                case 118:
                    returnData.verify_status = false;
                    // retry
                    break;
                default:
                    // Address is invalid
                    // res.info
                    // res.details
                    returnData.verify_status = false;
                    break;
            }

            string json = JsonConvert.SerializeObject(returnData);
            res.Write(json);
        }
        else
        {
            //res.Write("{\"verify_status\":false }");
            returnData.verify_status = false;
            returnData.verify_status_desc = "Email required";
            string json = JsonConvert.SerializeObject(returnData);
            res.Write(json);
        }
    }

    /*
    public void ProcessRequest2(HttpContext context)
    {
        HttpRequest Request = context.Request;
        HttpResponse Response = context.Response;
        //bool verify = ActiveUp.Net.Mail.SmtpValidator.Validate("myaya@greenwoodhall.com");
        //Response.ContentType = "application/json";
        //Response.Write("{\"success\":"+verify+"}");
        //ActiveUp.Net.Mail.MxRecordCollection col = ActiveUp.Net.Mail.SmtpValidator.GetMxRecords("greenwoodhall.com");
        //foreach (ActiveUp.Net.Mail.MxRecord l in col)
        //{
        //    Response.Write(l.Exchange.ToString() + "<br>");
        //}

        Chilkat.Email email = new Chilkat.Email();
        email.AddTo("", "a@gmail.com");
        email.FromAddress = "a@gmail.com";

        Chilkat.MailMan smtp = new Chilkat.MailMan();
        smtp.UnlockComponent("SGREENWOODMAILQ_FuY9K2d92R8F");
        smtp.SmtpHost = "smtp.gmail.com";
        smtp.SmtpPort = 465;
        smtp.SmtpSsl = true;


        Chilkat.StringArray saBadAddrs = new Chilkat.StringArray();
        bool success = smtp.VerifyRecipients(email, saBadAddrs);
        if (success != true)
        {
            Response.Write(smtp.LastErrorText + "<br>");
        }

        else
        {
            //  List the invalid email addresses:
            if (saBadAddrs.Count > 0)
            {
                int i;
                int n;
                n = saBadAddrs.Count;
                for (i = 0; i <= n - 1; i++)
                {
                    Response.Write(saBadAddrs.GetString(i) + "<br>");
                }

                //  Examine the SMTP session log to see how the email
                //  addresses were caught:
                //MessageBox.Show(mailman.SmtpSessionLog);
            }
        }

        Response.Write("{\"success\":" + success + "}");

        //SMTPMailSender client = new SMTPMailSender();
        
    }
    */
    public bool IsReusable
    {
        // To enable pooling, return true here.
        // This keeps the handler in memory.
        get { return false; }
    }
}
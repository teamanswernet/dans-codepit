﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class Result
{
    public Result()
    {
        verify_status = false;
        verify_output = String.Empty;
        authentication_status = String.Empty;
        limit_status = String.Empty;
        limit_desc = String.Empty;
        verify_status_desc = String.Empty;
    }
    public bool verify_status { get; set; }
    public string verify_output { get; set; }
    public string authentication_status { get; set; }
    public string limit_status { get; set; }
    public string limit_desc { get; set; }
    public string verify_status_desc { get; set; }
}
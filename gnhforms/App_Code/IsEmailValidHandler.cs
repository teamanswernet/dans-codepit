﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;


public class IsEmailValidHandler : IHttpHandler
{
	public IsEmailValidHandler()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void ProcessRequest(HttpContext context)
    {
        HttpRequest req = context.Request;
        HttpResponse res = context.Response;

        string apiUrl = "http://api.verify-email.org/api.php?";
        string apiUsername = "gnhnet";
        string apiPassword = "k1nd3rg@rt3n";
        string email = req.QueryString["email"];
        res.ContentType = "application/json";

        Result data = new Result();

        if (!String.IsNullOrEmpty(email))
        {
            
            
            WebClient webClient = new WebClient();
            string result = webClient.DownloadString(string.Format("{0}usr={1}&pwd={2}&check={3}", apiUrl, apiUsername, apiPassword, email));

            JObject objJSON = default(JObject);
            objJSON = JObject.Parse(result);

            /*string verify_status = String.Empty;
            string verify_output = String.Empty;
            string authentication_status = String.Empty;
            string limit_status = String.Empty;
            string limit_desc = String.Empty;
            string verify_status_desc = String.Empty;*/

            if (objJSON["verify_status"] != null){
                data.verify_status = Convert.ToBoolean(Convert.ToInt32(objJSON["verify_status"].ToString()));
                data.verify_output = objJSON["verify_status"].ToString();
            }
            //Response.Write(string.Format("The email address {0} is {1}", email, Convert.ToBoolean(Convert.ToInt32(objJSON["verify_status"].ToString())) ? "GOOD" : "BAD or cannot be verified"));

            if (objJSON["authentication_status"] != null)
                data.authentication_status = objJSON["authentication_status"].ToString();
            //Response.Write(string.Format("authentication_status: {0} (your authentication status: 1 = success; 0 = invalid user)", objJSON["authentication_status"].ToString()));

            if (objJSON["limit_status"] != null)
                data.limit_status = objJSON["limit_status"].ToString();
            //Response.Write(string.Format("limit_status: {0}", objJSON["limit_status"].ToString()));

            if (objJSON["limit_desc"] != null)
                data.limit_desc = objJSON["limit_desc"].ToString();
            //Response.Write(string.Format("limit_desc: {0}", objJSON["limit_desc"].ToString()));

            //if (objJSON["verify_status"] != null)
            //Response.Write(string.Format("verify_status: {0} (entered email is: 1 = OK; 0 = BAD)", objJSON["verify_status"].ToString()));

            if (objJSON["verify_status_desc"] != null)
                data.verify_status_desc = objJSON["verify_status_desc"].ToString();
            //Response.Write(string.Format("verify_status_desc: {0}", objJSON["verify_status_desc"].ToString()));



            //res.Write("{\"verify_status\":" + verify_status + ", \"authentication_status\":\"" + authentication_status + "\", \"limit_status\":\"" + limit_status + "\", \"verify_status_desc\":\"" + verify_status_desc + "\", \"limit_desc\":\"" + limit_desc + "\", \"verify_output\":\"" + verify_output + "\" }");
            
            //res.Write("{\"verify_status\":True }");
            string json = JsonConvert.SerializeObject(data);
            res.Write(json);
        }
        else
        {
            //res.Write("{\"verify_status\":false }");
            data.verify_status = false;
            data.verify_status_desc = "Email required";
            string json = JsonConvert.SerializeObject(data);
            res.Write(json);
        }
    }

    /*
    public void ProcessRequest2(HttpContext context)
    {
        HttpRequest Request = context.Request;
        HttpResponse Response = context.Response;
        //bool verify = ActiveUp.Net.Mail.SmtpValidator.Validate("myaya@greenwoodhall.com");
        //Response.ContentType = "application/json";
        //Response.Write("{\"success\":"+verify+"}");
        //ActiveUp.Net.Mail.MxRecordCollection col = ActiveUp.Net.Mail.SmtpValidator.GetMxRecords("greenwoodhall.com");
        //foreach (ActiveUp.Net.Mail.MxRecord l in col)
        //{
        //    Response.Write(l.Exchange.ToString() + "<br>");
        //}

        Chilkat.Email email = new Chilkat.Email();
        email.AddTo("", "a@gmail.com");
        email.FromAddress = "a@gmail.com";

        Chilkat.MailMan smtp = new Chilkat.MailMan();
        smtp.UnlockComponent("SGREENWOODMAILQ_FuY9K2d92R8F");
        smtp.SmtpHost = "smtp.gmail.com";
        smtp.SmtpPort = 465;
        smtp.SmtpSsl = true;


        Chilkat.StringArray saBadAddrs = new Chilkat.StringArray();
        bool success = smtp.VerifyRecipients(email, saBadAddrs);
        if (success != true)
        {
            Response.Write(smtp.LastErrorText + "<br>");
        }

        else
        {
            //  List the invalid email addresses:
            if (saBadAddrs.Count > 0)
            {
                int i;
                int n;
                n = saBadAddrs.Count;
                for (i = 0; i <= n - 1; i++)
                {
                    Response.Write(saBadAddrs.GetString(i) + "<br>");
                }

                //  Examine the SMTP session log to see how the email
                //  addresses were caught:
                //MessageBox.Show(mailman.SmtpSessionLog);
            }
        }

        Response.Write("{\"success\":" + success + "}");

        //SMTPMailSender client = new SMTPMailSender();
        
    }
    */
    public bool IsReusable
    {
        // To enable pooling, return true here.
        // This keeps the handler in memory.
        get { return false; }
    }
}
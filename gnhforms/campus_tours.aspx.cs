using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class campus_tours : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            List<DateTime> dtExclude = new List<DateTime>();


            dtExclude.Add(new DateTime(2017, 1, 3, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 1, 6, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 1, 13, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 2, 3, 0, 0, 0));

            dtExclude.Add(new DateTime(2017, 3, 3, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 3, 31, 0, 0, 0));

            dtExclude.Add(new DateTime(2017, 4, 7, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 4, 28, 0, 0, 0));

            dtExclude.Add(new DateTime(2017, 5, 19, 0, 0, 0));

            dtExclude.Add(new DateTime(2017, 5, 30, 0, 0, 0));

            dtExclude.Add(new DateTime(2017, 8, 22, 0, 0, 0));

            dtExclude.Add(new DateTime(2017, 9, 5, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 9, 8, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 9, 15, 0, 0, 0));

            dtExclude.Add(new DateTime(2017, 11, 21, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 11, 24, 0, 0, 0));

            dtExclude.Add(new DateTime(2017, 12, 8, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 12, 22, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 12, 29, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 12, 19, 0, 0, 0));
            dtExclude.Add(new DateTime(2017, 12, 26, 0, 0, 0));

            dtExclude.Add(new DateTime(2018, 1, 2, 0, 0, 0));

            DateTime maxDate = new DateTime(2018, 1, 31, 0, 0, 0);
            DateTime minDate = new DateTime(2017, 1, 1, 0, 0, 0);


            DateTime dtNow = DateTime.Now;
            DateTime dtCur = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 0, 0, 0);
            DateTime dtToday = DateTime.Today;
            //Response.Write(dtToday);
            //DateTime dtCur = new DateTime(minDate.Year, minDate.Month, minDate.Day, 0, 0, 0);

            while (true)
            {
                if (DateTime.Compare(dtCur, maxDate) <= 0)
                {
                    if (DateTime.Compare(minDate, dtCur) <= 0)
                    {
                        if (DateTime.Compare(dtToday, dtCur) == 0)
                        {

                        }
                        else
                        {
                            switch (dtCur.DayOfWeek)
                            {
                                // We only want evry Friday or Tuesday
                                case DayOfWeek.Tuesday:
                                case DayOfWeek.Friday:
                                    // Unless it is specifically a holiday
                                    if (!dtExclude.Contains(dtCur))
                                    {
                                        form_0005_fld_5.Items.Add(new ListItem(dtCur.ToString("MMMM d"), dtCur.ToString("MMMM d")));
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    break;
                }

                dtCur = dtCur.AddDays(1);
            }
            //end while

        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="campus_tours.aspx.cs" Inherits="campus_tours" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>HGTC Campus Tours Web Form</title>
        <%--<link rel="shortcut icon" type="image/x-icon" href="https://hgtc.actonsoftware.com/acton/image/favicon.png" />
        <link rel="icon" type="image/x-icon" href="https://hgtc.actonsoftware.com/acton/image/favicon.png" />--%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="og:title" content="HGTC Request Information Web Form" />
        <meta http-equiv="expires" value="Sat, 1 Jan 2000 11:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
        <script type="text/javascript" src="Scripts/jquery-2.1.0.min.js"></script>
        <script type="text/javascript" src="Scripts/jquery.maskedinput-1.3.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/campus_tours.css" />
        <script type="text/javascript">
            var r = 'l-005c';
            var doPrefill = 0;
            function doPrefillOnloadDone(ajax) { }
            function doPrefillOnload() {
                if (r && r.length > 0 && doPrefill) {
                    var parms = 'a=8952&id=0005&r=' + r + '&did=0005:d-0001&ts=' + (new Date().getTime());
                    new Ajax.Updater(
                        'prefillDiv',
                        '/acton/forms/prefillAjax.jsp',
                        {
                            method: 'get',
                            parameters: parms,
                            evalScripts: true,
                            onComplete: doPrefillOnloadDone
                        }
                        );
                }
            }
            function doOnLoad() {
                document.getElementById("ao_jstzo").value = new Date().getTimezoneOffset();
                var refURL = document.referrer;
                if (window.location.href.indexOf('embedded=1') !== -1) {
                    try {
                        var parentReferrer = parent.getReferrerHeader();
                        if (!parentReferrer || parentReferrer == '')
                            refURL = getParameter("refUrl");
                        else refURL = parentReferrer;
                    }
                    catch (e) {
                        refURL = getParameter("refUrl");
                    }
                }
                if (refURL != null)
                    document.getElementById("ao_refurl").value = refURL;
                doPrefillOnload();

                $("#form_0005_fld_1_fn").attr("name", "First Name");
                $("#form_0005_fld_1_ln").attr("name", "Last Name");
                $("#form_0005_fld_2_Street").attr("name", "Home Street");
                $("#form_0005_fld_2_City").attr("name", "Home City");
                $("#form_0005_fld_2_State").attr("name", "Home State");
                $("#form_0005_fld_2_Zip").attr("name", "Home Postal Code");
                $("#form_0005_fld_2_Country").attr("name", "Home Country");
                $("#form_0005_fld_3").attr("name", "E-mail Address");
                $("#form_0005_fld_4_1").attr("name", "Home Phone");
                $("#form_0005_fld_4_2").attr("name", "Cell Phone");
                $("#form_0005_fld_5").attr("name", "Tour Date");
                $("#form_0005_fld_6").attr("name", "Tour Location");
                $("#form_0005_fld_7").attr("name", "Type of Student");
                $("#form_0005_fld_8").attr("name", "Major Area Interested In");
                $("#form_0005_fld_9").attr("name", "Start Date");
                $("#form_0005_fld_10").attr("name", "Accommodations");
                $("#form_0005_fld_11").attr("name", "Accommodation Details");
                $("#form_0005_fld_12").attr("name", "Additional Comments");
            }

            function openWindow(url) {
                window.open(url, "popup");
            }

            function ValidateInputs() {


                Page_ClientValidate();//Validate the validators using microsofts validation

                var validators = Page_Validators;
                var isValid = true;

                for (var i = 0; i < validators.length; i++) {
                    var validator = validators[i];
                    if (!validator.isvalid) {
                        $("#" + validator.controltovalidate).addClass("validation-error");
                        //document.getElementById(validator.controltovalidate).setAttribute("style", "border:solid 1px red;");
                        isValid = false;
                    }
                    else {
                        $("#" + validator.controltovalidate).removeClass("validation-error");
                    }
                }

                // The form on the iFrame does not behave the same as when directly visited
                // For this reason we disable the button and hide the form on a validated submit
                // This is a temporary fix since we need to ensure the form was actually submitted as well

                if (isValid) {
                    //$("#form_0005_ao_submit_input").attr("disabled", "disabled");
                    //$("#form_0005_ao_submit_input").val("Submitted");
                    //$("#form_0005_ao_submit_input").css("background-color", "#8cb6de")

                    $(".ao_tbl_container").hide();
                    $(".ao_tbl_submitted").show();
                }
            }

            $(function () {
                $("#form_0005_fld_4_1").mask("(999) 999-9999");
                $("#form_0005_fld_4_2").mask("(999) 999-9999");
                $("#form_0005_fld_2_Zip").mask("99999");
                $("#form_0005_fld_9").val("Summer 2017");
                $("#form_0005_fld_2_State").val("SC");
                $("#form_0005_fld_2_Country").val("US");

                $("#form_0005_ao_submit_input").click(function (e) {

                    ValidateInputs();

                    $("#ao_bot").val("nope");

                    // document.getElementById('ao_iframe').value = window.parent ? window.parent.location.href : "";
                });
            });
        </script>
        <!-- Google Tag Manager -->
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-MJDZNPH');
        </script>
        <!-- End Google Tag Manager -->
    </head>
    <body onload="doOnLoad()">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MJDZNPH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <form id="form_0005" runat="server">
            <input type="hidden" name="ao_a" value="8952">
            <input type="hidden" name="ao_f" value="0005">
            <input type="hidden" name="ao_d" value="0005:d-0001">
            <input type="hidden" name="ao_p" id="ao_p" value="0">
            <input type="hidden" name="ao_jstzo" id="ao_jstzo" value="">
            <input type="hidden" name="ao_refurl" id="ao_refurl" value="">
            <input type="hidden" name="ao_cuid" value="l-005c">
            <input type="hidden" name="ao_srcid" value="">
            <input type="hidden" name="ao_nc" value="">
            <input type="hidden" name="ao_pf" value="0">
            <input type="hidden" name="ao_bot" id="ao_bot" value="yes">
            <input type="hidden" name="ao_iframe" id="ao_iframe" value="">
            <input type="hidden" name="ao_camp" value="">
            <input type="hidden" name="ao_campid" value="">
            <!-- -------------------------------------------------------------------------------------------- -->
            <style type="text/css">
                table, tr, td, body, p, div, span, textarea, input, select, a, ul, ol {
                    font-size: 11pt;
                }
            </style>
            <!-- -------------------------------------------------------------------------------------------- -->
            <div id="ao_alignment_container" align="center">
                <table class="ao_tbl_container" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="left" colspan="2">
                            <h3>Request a Tour: </h3>
                        </td>
                    </tr>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <table cellspacing="4" cellpadding="1" border="0" width="100%">
                                        <!-- FIRSTNAME -->

                                        <tr>
                                            <td class="formFieldLabel">First Name<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_1_fn" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <input type="text" class="l6e formFieldText formFieldMediumRight"
                                                    id="form_0005_fld_1_fn" name="First Name" placeholder="First Name" value="" runat="server">
                                            </td>
                                        </tr>


                                        <!-- LAST NAME -->
                                        <tr>
                                            <td class="formFieldLabel">Last Name<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_1_ln" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <input type="text" class="l6e formFieldText formFieldMediumRight"
                                                    id="form_0005_fld_1_ln" name="Last Name" placeholder="Last Name" value="" runat="server">
                                            </td>
                                        </tr>
                                    

                                        <!-- ADDRESS -->
                                        <tr>
                                            <td class="formFieldLabel">Address<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_2_Street" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <input type="text" class="l6e formFieldText formFieldMediumRight"
                                                    id="form_0005_fld_2_Street" name="Home Street" placeholder="Address" value="" runat="server">
                                            </td>
                                        </tr>


                                        <!-- CITY -->
                                        <tr>
                                            <td class="formFieldLabel">City<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_2_City" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <input type="text" class="l6e formFieldText formFieldMediumRight"
                                                    id="form_0005_fld_2_City" name="Home City" placeholder="City" value="" runat="server">
                                            </td>
                                        </tr>


                                        <!-- STATE -->
                                        <tr>
                                            <td class="formFieldLabel">State<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_2_State" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <select class="formFieldSelect formFieldMediumRight" name="Home State" id="form_0005_fld_2_State" runat="server">
                                                    <option value="">---Select---</option>
                                                    <option value="AL">Alabama</option>
                                                    <option value="AK">Alaska</option>
                                                    <option value="AZ">Arizona</option>
                                                    <option value="AR">Arkansas</option>
                                                    <option value="CA">California</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="CT">Connecticut</option>
                                                    <option value="DE">Delaware</option>
                                                    <option value="DC">District of Columbia</option>
                                                    <option value="FL">Florida</option>
                                                    <option value="GA">Georgia</option>
                                                    <option value="HI">Hawaii</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="IL">Illinois</option>
                                                    <option value="IN">Indiana</option>
                                                    <option value="IA">Iowa</option>
                                                    <option value="KS">Kansas</option>
                                                    <option value="KY">Kentucky</option>
                                                    <option value="LA">Louisiana</option>
                                                    <option value="ME">Maine</option>
                                                    <option value="MD">Maryland</option>
                                                    <option value="MA">Massachusetts</option>
                                                    <option value="MI">Michigan</option>
                                                    <option value="MN">Minnesota</option>
                                                    <option value="MS">Mississippi</option>
                                                    <option value="MO">Missouri</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NV">Nevada</option>
                                                    <option value="NH">New Hampshire</option>
                                                    <option value="NJ">New Jersey</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="NY">New York</option>
                                                    <option value="NC">North Carolina</option>
                                                    <option value="ND">North Dakota</option>
                                                    <option value="OH">Ohio</option>
                                                    <option value="OK">Oklahoma</option>
                                                    <option value="OR">Oregon</option>
                                                    <option value="PA">Pennsylvania</option>
                                                    <option value="PR">Puerto Rico</option>
                                                    <option value="RI">Rhode Island</option>
                                                    <option value="SC">South Carolina</option>
                                                    <option value="SD">South Dakota</option>
                                                    <option value="TN">Tennessee</option>
                                                    <option value="TX">Texas</option>
                                                    <option value="UT">Utah</option>
                                                    <option value="VT">Vermont</option>
                                                    <option value="VA">Virginia</option>
                                                    <option value="WA">Washington</option>
                                                    <option value="WV">West Virginia</option>
                                                    <option value="WI">Wisconsin</option>
                                                    <option value="WY">Wyoming</option>
                                                    <option value="AB">Alberta</option>
                                                    <option value="BC">British Columbia</option>
                                                    <option value="MB">Manitoba</option>
                                                    <option value="NB">New Brunswick</option>
                                                    <option value="NL">Newfoundland and Labrador</option>
                                                    <option value="NS">Nova Scotia</option>
                                                    <option value="ON">Ontario</option>
                                                    <option value="PE">Prince Edward Island</option>
                                                    <option value="QC">Quebec</option>
                                                    <option value="SK">Saskatchewan</option>
                                                    <option value="INT">International/Other</option>
                                                </select>
                                            </td>
                                        </tr>

                                        <!-- ZIP -->
                                        <tr>
                                            <td class="formFieldLabel">Zipcode<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_2_Zip" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <input type="text" class="l6e formFieldText formFieldMediumRight"
                                                    id="form_0005_fld_2_Zip" name="Home Postal Code" placeholder="Zipcode" value="" runat="server">
                                            </td>
                                        </tr>


                                        <!-- COUNTRY -->
                                        <tr>
                                            <td class="formFieldLabel">Country<b style="color: #FF0000; cursor: default" title="Required Field">
                                            

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <select class="formFieldSelect formFieldMediumRight" name="Home Country" id="form_0005_fld_2_Country">
                                                    <option value="">---Select---</option>
                                                    <option value="AF">Afghanistan</option>
                                                    <option value="AL">Albania</option>
                                                    <option value="DZ">Algeria</option>
                                                    <option value="AS">American Samoa</option>
                                                    <option value="AD">Andorra</option>
                                                    <option value="AO">Angola</option>
                                                    <option value="AI">Anguilla</option>
                                                    <option value="AQ">Antarctica</option>
                                                    <option value="AG">Antigua and Barbuda</option>
                                                    <option value="AR">Argentina</option>
                                                    <option value="AM">Armenia</option>
                                                    <option value="AW">Aruba</option>
                                                    <option value="AU">Australia</option>
                                                    <option value="AT">Austria</option>
                                                    <option value="AZ">Azerbaijan</option>
                                                    <option value="BS">Bahamas</option>
                                                    <option value="BH">Bahrain</option>
                                                    <option value="BD">Bangladesh</option>
                                                    <option value="BB">Barbados</option>
                                                    <option value="BY">Belarus</option>
                                                    <option value="BE">Belgium</option>
                                                    <option value="BZ">Belize</option>
                                                    <option value="BJ">Benin</option>
                                                    <option value="BM">Bermuda</option>
                                                    <option value="BT">Bhutan</option>
                                                    <option value="BO">Bolivia</option>
                                                    <option value="BQ">Bonaire</option>
                                                    <option value="BA">Bosnia</option>
                                                    <option value="BW">Botswana</option>
                                                    <option value="BV">Bouvet Island</option>
                                                    <option value="BR">Brazil</option>
                                                    <option value="IO">British Indian Ocean Territory</option>
                                                    <option value="BN">Brunei</option>
                                                    <option value="BG">Bulgaria</option>
                                                    <option value="BF">Burkina Faso</option>
                                                    <option value="BI">Burundi</option>
                                                    <option value="KH">Cambodia</option>
                                                    <option value="CM">Cameroon</option>
                                                    <option value="CA">Canada</option>
                                                    <option value="CV">Cape Verde</option>
                                                    <option value="KY">Cayman Islands</option>
                                                    <option value="CF">Central African Republic</option>
                                                    <option value="TD">Chad</option>
                                                    <option value="CL">Chile</option>
                                                    <option value="CN">China</option>
                                                    <option value="CX">Christmas Island</option>
                                                    <option value="CC">Cocos Islands</option>
                                                    <option value="CO">Colombia</option>
                                                    <option value="KM">Comoros</option>
                                                    <option value="CG">Congo</option>
                                                    <option value="CD">Congo, Dem. Rep.</option>
                                                    <option value="CK">Cook Islands</option>
                                                    <option value="CR">Costa Rica</option>
                                                    <option value="HR">Croatia</option>
                                                    <option value="CU">Cuba</option>
                                                    <option value="CW">Curaçao</option>
                                                    <option value="CY">Cyprus</option>
                                                    <option value="CZ">Czech Republic</option>
                                                    <option value="DK">Denmark</option>
                                                    <option value="DJ">Djibouti</option>
                                                    <option value="DM">Dominica</option>
                                                    <option value="DO">Dominican Republic</option>
                                                    <option value="EC">Ecuador</option>
                                                    <option value="EG">Egypt</option>
                                                    <option value="SV">El Salvador</option>
                                                    <option value="GQ">Equatorial Guinea</option>
                                                    <option value="ER">Eritrea</option>
                                                    <option value="EE">Estonia</option>
                                                    <option value="ET">Ethiopia</option>
                                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                                    <option value="FO">Faroe Islands</option>
                                                    <option value="FJ">Fiji</option>
                                                    <option value="FI">Finland</option>
                                                    <option value="FR">France</option>
                                                    <option value="GF">French Guiana</option>
                                                    <option value="PF">French Polynesia</option>
                                                    <option value="TF">French Southern Territories</option>
                                                    <option value="GA">Gabon</option>
                                                    <option value="GM">Gambia</option>
                                                    <option value="GE">Georgia</option>
                                                    <option value="DE">Germany</option>
                                                    <option value="GH">Ghana</option>
                                                    <option value="GI">Gibraltar</option>
                                                    <option value="GR">Greece</option>
                                                    <option value="GL">Greenland</option>
                                                    <option value="GD">Grenada</option>
                                                    <option value="GP">Guadeloupe</option>
                                                    <option value="GU">Guam</option>
                                                    <option value="GT">Guatemala</option>
                                                    <option value="GG">Guernsey</option>
                                                    <option value="GN">Guinea</option>
                                                    <option value="GW">Guinea-Bissau</option>
                                                    <option value="GY">Guyana</option>
                                                    <option value="HT">Haiti</option>
                                                    <option value="HM">Heard Island</option>
                                                    <option value="HN">Honduras</option>
                                                    <option value="HK">Hong Kong</option>
                                                    <option value="HU">Hungary</option>
                                                    <option value="IS">Iceland</option>
                                                    <option value="IN">India</option>
                                                    <option value="ID">Indonesia</option>
                                                    <option value="IR">Iran</option>
                                                    <option value="IQ">Iraq</option>
                                                    <option value="IE">Ireland</option>
                                                    <option value="IM">Isle of Man</option>
                                                    <option value="IL">Israel</option>
                                                    <option value="IT">Italy</option>
                                                    <option value="CI">Ivory Coast</option>
                                                    <option value="JM">Jamaica</option>
                                                    <option value="JP">Japan</option>
                                                    <option value="JE">Jersey</option>
                                                    <option value="JO">Jordan</option>
                                                    <option value="KZ">Kazakhstan</option>
                                                    <option value="KE">Kenya</option>
                                                    <option value="KI">Kiribati</option>
                                                    <option value="KR">Korea</option>
                                                    <option value="KP">Korea, DPRK</option>
                                                    <option value="KW">Kuwait</option>
                                                    <option value="KG">Kyrgyzstan</option>
                                                    <option value="LA">Laos</option>
                                                    <option value="LV">Latvia</option>
                                                    <option value="LB">Lebanon</option>
                                                    <option value="LS">Lesotho</option>
                                                    <option value="LR">Liberia</option>
                                                    <option value="LY">Libya</option>
                                                    <option value="LI">Liechtenstein</option>
                                                    <option value="LT">Lithuania</option>
                                                    <option value="LU">Luxembourg</option>
                                                    <option value="MO">Macao</option>
                                                    <option value="MK">Macedonia</option>
                                                    <option value="MG">Madagascar</option>
                                                    <option value="MW">Malawi</option>
                                                    <option value="MY">Malaysia</option>
                                                    <option value="MV">Maldives</option>
                                                    <option value="ML">Mali</option>
                                                    <option value="MT">Malta</option>
                                                    <option value="MH">Marshall Islands</option>
                                                    <option value="MQ">Martinique</option>
                                                    <option value="MR">Mauritania</option>
                                                    <option value="MU">Mauritius</option>
                                                    <option value="YT">Mayotte</option>
                                                    <option value="MX">Mexico</option>
                                                    <option value="FM">Micronesia, Fed.</option>
                                                    <option value="MD">Moldova</option>
                                                    <option value="MC">Monaco</option>
                                                    <option value="MN">Mongolia</option>
                                                    <option value="ME">Montenegro</option>
                                                    <option value="MS">Montserrat</option>
                                                    <option value="MA">Morocco</option>
                                                    <option value="MZ">Mozambique</option>
                                                    <option value="MM">Myanmar</option>
                                                    <option value="NA">Namibia</option>
                                                    <option value="NR">Nauru</option>
                                                    <option value="NP">Nepal</option>
                                                    <option value="NL">Netherlands</option>
                                                    <option value="NC">New Caledonia</option>
                                                    <option value="NZ">New Zealand</option>
                                                    <option value="NI">Nicaragua</option>
                                                    <option value="NE">Niger</option>
                                                    <option value="NG">Nigeria</option>
                                                    <option value="NU">Niue</option>
                                                    <option value="NF">Norfolk Island</option>
                                                    <option value="MP">Northern Marianas</option>
                                                    <option value="NO">Norway</option>
                                                    <option value="OM">Oman</option>
                                                    <option value="PK">Pakistan</option>
                                                    <option value="PW">Palau</option>
                                                    <option value="PS">Palestinian Terr.</option>
                                                    <option value="PA">Panama</option>
                                                    <option value="PG">Papua New Guinea</option>
                                                    <option value="PY">Paraguay</option>
                                                    <option value="PE">Peru</option>
                                                    <option value="PH">Philippines</option>
                                                    <option value="PN">Pitcairn</option>
                                                    <option value="PL">Poland</option>
                                                    <option value="PT">Portugal</option>
                                                    <option value="PR">Puerto Rico</option>
                                                    <option value="QA">Qatar</option>
                                                    <option value="RO">Romania</option>
                                                    <option value="RU">Russia</option>
                                                    <option value="RW">Rwanda</option>
                                                    <option value="RE">Réunion</option>
                                                    <option value="BL">Saint Barthélemy</option>
                                                    <option value="SH">Saint Helena</option>
                                                    <option value="KN">Saint Kitts and Nevis</option>
                                                    <option value="LC">Saint Lucia</option>
                                                    <option value="MF">Saint Martin (French)</option>
                                                    <option value="PM">Saint Pierre and Miquelon</option>
                                                    <option value="VC">Saint Vincent</option>
                                                    <option value="WS">Samoa</option>
                                                    <option value="SM">San Marino</option>
                                                    <option value="ST">Sao Tome & Principe</option>
                                                    <option value="SA">Saudi Arabia</option>
                                                    <option value="SN">Senegal</option>
                                                    <option value="RS">Serbia</option>
                                                    <option value="SC">Seychelles</option>
                                                    <option value="SL">Sierra Leone</option>
                                                    <option value="SG">Singapore</option>
                                                    <option value="SX">Sint Maarten (Dutch)</option>
                                                    <option value="SK">Slovakia</option>
                                                    <option value="SI">Slovenia</option>
                                                    <option value="SB">Solomon Islands</option>
                                                    <option value="SO">Somalia</option>
                                                    <option value="ZA">South Africa</option>
                                                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                    <option value="SS">South Sudan</option>
                                                    <option value="ES">Spain</option>
                                                    <option value="LK">Sri Lanka</option>
                                                    <option value="SD">Sudan</option>
                                                    <option value="SR">Suriname</option>
                                                    <option value="SJ">Svalbard</option>
                                                    <option value="SZ">Swaziland</option>
                                                    <option value="SE">Sweden</option>
                                                    <option value="CH">Switzerland</option>
                                                    <option value="SY">Syria</option>
                                                    <option value="TW">Taiwan</option>
                                                    <option value="TJ">Tajikistan</option>
                                                    <option value="TZ">Tanzania</option>
                                                    <option value="TH">Thailand</option>
                                                    <option value="TL">Timor-Leste</option>
                                                    <option value="TG">Togo</option>
                                                    <option value="TK">Tokelau</option>
                                                    <option value="TO">Tonga</option>
                                                    <option value="TT">Trinidad and Tobago</option>
                                                    <option value="TN">Tunisia</option>
                                                    <option value="TR">Turkey</option>
                                                    <option value="TM">Turkmenistan</option>
                                                    <option value="TC">Turks and Caicos</option>
                                                    <option value="TV">Tuvalu</option>
                                                    <option value="AE">U.A.E.</option>
                                                    <option value="UG">Uganda</option>
                                                    <option value="UA">Ukraine</option>
                                                    <option value="GB">United Kingdom</option>
                                                    <option value="US">United States</option>
                                                    <option value="UM">United States Minor Outlying Islands</option>
                                                    <option value="UY">Uruguay</option>
                                                    <option value="UZ">Uzbekistan</option>
                                                    <option value="VU">Vanuatu</option>
                                                    <option value="VA">Vatican City</option>
                                                    <option value="VE">Venezuela</option>
                                                    <option value="VN">Viet Nam</option>
                                                    <option value="VG">Virgin Islands, British</option>
                                                    <option value="VI">Virgin Islands, U.S.</option>
                                                    <option value="WF">Wallis and Futuna</option>
                                                    <option value="EH">Western Sahara</option>
                                                    <option value="YE">Yemen</option>
                                                    <option value="ZM">Zambia</option>
                                                    <option value="ZW">Zimbabwe</option>
                                                    <option value="AX">Åland Islands</option>
                                                </select>
                                            </td>
                                        </tr>


                                        <!-- HOME PHONE -->
                                        <tr>
                                            <td class="formFieldLabel">Phone<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_4_1" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <input type="text" class="l6e formFieldText formFieldMediumRight"
                                                    id="form_0005_fld_4_1" name="Home Phone" placeholder="Phone" value="" runat="server">
                                            </td>
                                        </tr>


                                        <!-- CELL PHONE -->
                                        <tr style="display: none;">
                                            <td class="formFieldLabel">Cell Phone<b style="color: #FF0000; cursor: default" title="Required Field">
                                            
                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <input type="text" class="l6e formFieldText formFieldMediumRight"
                                                    id="form_0005_fld_4_2" name="Cell Phone" placeholder="Cell Phone" value="" runat="server">
                                            </td>
                                        </tr>

                                        <!-- EMAIL -->
                                        <tr>
                                            <td class="formFieldLabel">Email<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_3"
                                                    EnableClientScript="true" Display="Dynamic"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator runat="server" ID="revEmail"
                                                    Display="Dynamic" ControlToValidate="form_0005_fld_3"
                                                    EnableClientScript="true"
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">* Invalid email format<br /></asp:RegularExpressionValidator>

                                                <asp:CustomValidator ID="cvEmail" runat="server" ControlToValidate="form_0005_fld_3"
                                                    Display="Dynamic"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true" ClientValidationFunction="isEmailValid">* Email does not exist!<br /></asp:CustomValidator>

                                                <asp:HiddenField ID="emailValidated" runat="server" />
                                                <asp:HiddenField ID="emailValidatedResult" runat="server" />
                                                <script type="text/javascript">
                                                    function isEmailValid(source, arguments) {
                                                        // This submits the Email Validator
                                                        // This patch makes it so we do not validate the email if it's not a valid email address
                                                        // 
                                                        // I also implemented 2 hidden fields to not validate the same email address multiple times
                                                        // This won't help if the user refreshes, but should stop it from using excessive credits

                                                        var isValid;

                                                        var revEmail = document.getElementById("<%=revEmail.ClientID%>");
                                                        ValidatorValidate(revEmail);

                                                        if (revEmail.isvalid) {

                                                            var emailToValidate = arguments.Value;
                                                            var emailValidated = document.getElementById("<%=emailValidated.ClientID%>");
                                                            var emailValidatedResult = document.getElementById("<%=emailValidatedResult.ClientID%>");
                                                            var valType = "";

                                                            var doEmailValidator = true; // Change to false if we want to bypass the PAID email validator
                                                            if (doEmailValidator && (emailValidated.value != emailToValidate)) {
                                                                $.ajax({
                                                                    type: "POST",
                                                                    contentType: "application/json; charset=utf-8",
                                                                    url: "EmailValidatorDotNetHandler.ashx?email=" + arguments.Value,
                                                                    data: "{'email': '" + arguments.Value + "'}",
                                                                    dataType: "json",
                                                                    async: false,
                                                                    success: function (data) {
                                                                        isValid = data.verify_status;
                                                                    }
                                                                });
                                                                valType = "API";
                                                            } else {
                                                                if (!doEmailValidator) { emailValidatedResult.value = "true"; } // If the PAID email validator is bypassed, we ensure no errors are caught

                                                                if (emailValidatedResult.value == "true") { isValid = true; } else { isValid = false; }
                                                                valType = "SES";
                                                            }

                                                            emailValidated.value = emailToValidate;
                                                            emailValidatedResult.value = isValid;

                                                            // DeBug
                                                            // alert(emailToValidate + "\n" + emailValidated.value + "\n" + emailValidatedResult.value + "\n" + valType);

                                                        } else {
                                                            isValid = true;
                                                        }


                                                        arguments.IsValid = isValid;
                                                    }

                                                </script>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <input type="email" class="l6e formFieldText formFieldMediumRight"
                                                    id="form_0005_fld_3" name="E-mail Address" placeholder="Email" value="" runat="server">
                                            </td>
                                        </tr>


                                        <!-- TOUR DATE -->
                                        <tr>
                                            <td class="formFieldLabel">Tour Date<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_5" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <select class="formFieldSelect formFieldMediumRight" name="Tour Date" id="form_0005_fld_5" 
                                                    runat="server" placeholder="Tour Date">

                                                    <%--<option value="September 23">September 23</option>
                                                    <option value="September 30">September 30</option>
                                                    <option value="October 7">October 7</option>
                                                    <option value="October 14">October 14</option>
                                                    <option value="October 21">October 21</option>
                                                    <option value="October 28">October 28</option>--%>
                                                </select>
                                            </td>
                                        </tr>


                                        <!-- TOUR DATE -->
                                        <tr>
                                            <td class="formFieldLabel">Tour Location<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_6" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <select class="formFieldSelect formFieldMediumRight" name="Tour Location" 
                                                    id="form_0005_fld_6" runat="server">
                                                    <option value="ConwayCampus">Conway Campus</option>
                                                    <option value="GrandStrandCampus">Grand Strand Campus</option>
                                                </select>
                                            </td>
                                        </tr>


                                        <!-- STUDENT TYPE -->
                                        <tr>
                                            <td class="formFieldLabel">Student Type<b style="color: #FF0000; cursor: default" title="Required Field">

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <select class="formFieldSelect formFieldMediumRight" name="Type of Student" 
                                                    id="form_0005_fld_7" runat="server">
                                                    <option value="New">New</option>
                                                    <option value="Returning">Returning</option>
                                                    <option value="Transfer">Transfer</option>
                                                    <option value="High School">High School</option>
                                                    <option value="Transient/Summer">Transient/Summer</option>
                                                    <option value="Health Science">Health Science</option>
                                                </select>
                                            </td>
                                        </tr>


                                        <!-- Major Area Interested In -->
                                        <tr>
                                            <td class="formFieldLabel">Area of Interest<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_8" Display="Dynamic" EnableClientScript="true" 
                                                    ValidationGroup="hgtc" SetFocusOnError="true">*</asp:RequiredFieldValidator> 
                                                </b>

                                                <%--<a class="link" href="javascript:void(0)" onclick="javascript:openWindow('https://www.hgtc.edu/academics/academic_departments/')">See all programs</a>--%>
                                            </td>
                                            <td class="formFieldLabel">
                                                <select class="formFieldSelect formFieldMediumRight" name="Major Area Interested In" 
                                                    id="form_0005_fld_8" runat="server">
                                                    <option value="Advanced ManufacturingIndustrial">Advanced Manufacturing/Industrial</option>
                                                    <option value="Arts,Sciences,Transfer">Arts, Sciences, Transfer</option>
                                                    <option value="Business">Business </option>
                                                    <option value="CulinaryArts">Culinary Arts</option>
                                                    <option value="Engineering">Engineering</option>
                                                    <option value="HealthScienceTechnology">Health Science Technology</option>
                                                    <option value="InformationTechnology&DigitalArts">Information Technology & Digital Arts</option>
                                                    <option value="NaturalResourcesTechnology">Natural Resources Technology</option>
                                                    <option value="OccupationalTechnology">Occupational Technology</option>
                                                    <option value="PersonalCare">Personal Care </option>
                                                    <option value="PublicServiceTechnology">Public Service Technology</option>
                                                </select>

                                            
                                            </td>
                                        </tr>


                                        <!-- START DATE -->
                                        <tr>
                                            <td class="formFieldLabel">Start Date<b style="color: #FF0000; cursor: default" title="Required Field">
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="form_0005_fld_9" Display="Dynamic" EnableClientScript="true"
                                                    ValidationGroup="hgtc" CssClass="error" SetFocusOnError="true">*</asp:RequiredFieldValidator>

                                                </b>
                                            </td>
                                            <td class="formFieldLabel">
                                                <select class="formFieldSelect formFieldMediumRight" name="Start Date" id="form_0005_fld_9" runat="server">
                                                    <option value=""></option>
                                                    <option value="Summer 2017">Summer 2017</option>
                                                    <option value="Fall 2017">Fall 2017</option>
                                                    <option value="Spring 2018">Spring 2018</option>
                                                </select>
                                            </td>
                                        </tr>

                                        <!-- ACCOMODATIONS -->
                                        <tr>
                                            <td class="formFieldLabel">Do you need Accomodations?</td>
                                            <td class="formFieldLabel">
                                                <select class="formFieldSelect formFieldMediumRight" name="Accommodations" id="form_0005_fld_10" runat="server">
                                                    <option value="Yes">Yes</option>
                                                    <option value="No">No</option>
                                                </select>

                                            </td>
                                        </tr>


                                        <!-- ACCOMODATIONS -->
                                        <tr>
                                            <td class="formFieldLabel" valign="top">What kind?
                                            </td>
                                            <td class="formFieldLabel">
                                                <textarea class="formTextArea formTextAreaSmall formTextAreaWidthLarge" id="form_0005_fld_11" runat="server" name="Accommodation Details" placeholder="What kind of Accomodations?"></textarea>
                                            </td>
                                        </tr>
                                        <!-- ACCOMODATIONS -->
                                        <tr>
                                            <td class="formFieldLabel" valign="top">Additional Comments
                                            </td>
                                            <td class="formFieldLabel">
                                                <textarea class="formTextArea formTextAreaSmall formTextAreaWidthLarge" id="form_0005_fld_12" runat="server" name="Additional Comments" placeholder="Additional Comments"></textarea>
                                            </td>
                                        </tr>

                                    </table>
                                </div>

                            </div>
                        </td>
                    </tr>
                    <!-- ADDITIONAL COMMENTS -->
                    <%--<tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <div class="formFieldLabel">Additional Comments</div>
                                    <textarea class="formTextArea formTextAreaSmall formTextAreaWidthLarge" 
                                        id="form_0005_fld_10" runat="server" name="Additional Comments" placeholder="Additional Comments"></textarea>
                                </div>
                            </div>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="center">
                            <div align="left">
                                <div class="formField">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" id="form_0005_ao_submit_button">                                            
                                                <asp:Button ID="form_0005_ao_submit_input"
                                                    runat="server"
                                                    CssClass="submit" 
                                                    CausesValidation="true" 
                                                    ValidationGroup="hgtc" 
                                                    Text="Submit" 
                                                    UseSubmitBehavior="true" />

    <%--                                            <asp:Button ID="form_0005_ao_submit_input"
                                                    ClientIDMode="Static"
                                                    runat="server"
                                                    CssClass="submit" 
                                                    CausesValidation="true"
                                                    ValidationGroup="hgtc" 
                                                    Text="Submit"
                                                    UseSubmitBehavior="true"  />--%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="formNegCap">
                        <td>
                            <input type="text" id="ao_form_neg_cap" name="ao_form_neg_cap" value="">
                        </td>
                    </tr>
                </table>
                <table class="ao_tbl_submitted" border="0" cellspacing="0" cellpadding="0" style="display: none;">
                    <tr>
                        <td class="ao_tbl_cell" style="padding-left: 10px; padding-right: 10px" align="left" colspan="2">
                            <h3>Thank you, your information has been submitted.</h3>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
        <div id="prefillDiv" style="display: none"></div>
        <script type="text/javascript">
            /* <![CDATA[ */
            //Avoid using document.write()
            //document.write('<img src="https://ci53.actonsoftware.com/acton/bn/8952/visitor.gif?ts=' + new Date().getTime() + '&ref=' + escape(document.referrer) + '">');

            // The above is not advised, the below doesn't work
            //var sNew = document.createElement("script2");
            //sNew.async = true;
            //sNew.src = "https://ci53.actonsoftware.com/acton/bn/8952/visitor.gif?ts=" + new Date().getTime() + "&ref=" + escape(document.referrer);
            //var s0 = document.getElementsByTagName('script2')[0];
            //s0.parentNode.insertBefore(sNew, s0);

            var aoAccountId = '8952';
            var aoCookieMode = 'STANDARD';
            var aoCookieDomain = 'actonsoftware.com';
            var aoServerContext = 'https://hgtc.actonsoftware.com/acton';
            /* ]]> */
        </script>
    </body>
</html>
